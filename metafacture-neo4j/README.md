# metafacture-neo4j

Provides commands for preparing for and writing to a [Neo4j](https://neo4j.com) instance:

* [encode-neo4j](#encode-neo4j): Encodes data as csv files suitable for batch uploads to a Neo4j database
* [index-neo4j](#index-neo4j): Indexes nodes and relationships in Neo4j
* [write-neo4j](#write-neo4j): Writes csv files for batch uploading to a new Neo4j database.


## encode-neo4j

*Encodes records as csv files for batch uploading them to a new Neo4j-database. As the headers of the csv files are hardcoded, it is not ready to be used in a broader context.*

* Implementation: [org.swissbib.linked.neo4j.NeoEncoder](src/main/java/org/swissbib/linked/neo4j/NeoEncoder.java)
* In: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)
* Out: `java.lang.String`

Example: [Graph visualisation of the GND](https://github.com/guenterh/gndHackathon2016/blob/master/examples/gh/hackathonGND)


## index-neo4j

*Indexes fields in Neo4j. Because the selection of the fields which are to be indexed is hardcoded, the benefit of this command outside our admittedly narrow scope is somewhat limited.*

* Implementation: [org.swissbib.linked.neo4j.NeoIndexer](src/main/java/org/swissbib/linked/neo4j/NeoIndexer.java)
* In: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)
* Out: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)
* Options:
    * batchSize: Size of batch upload for Neo4j
    * dbDir: Path to Neo4j database


## write-neo4j

*Writes csv files for batch uploading to a new Neo4j database. Intended to be used in junction with index-neo4j.*

* Implementation: [org.swissbib.linked.neo4j.NeoWriter](src/main/java/org/swissbib/linked/neo4j/NeoWriter.java)
* In: `java.lang.Object`
* Out: `java.lang.Void`
* Options:
    * csvDir: Path to the output directory
    * csvFileLength: Numbers of records in one dedicated CSV file
    * batchWriteSize: Maximal number of records of the same category

Example: [Graph visualisation of the GND](https://github.com/guenterh/gndHackathon2016/blob/master/examples/gh/hackathonGND)