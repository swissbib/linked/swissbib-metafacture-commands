# Extensions for [`metafacture-json`](https://github.com/metafacture/metafacture-core/tree/master/metafacture-json)

Provides commands for handling JSON:

* [decode-json](#decode-json): Parses JSON files
* [read-json-object](#read-json-object): Reads in a JSON file and splits it at the end of the root object / array.


## decode-json

*Parses JSON. Preferably used in conjunction with [read-json-object](#read-json-object)*

* Implementation: [org.swissbib.linked.json.JsonDecoder](src/main/java/org/swissbib/linked/json/JsonDecoder.java)
* In: `java.io.Reader`
* Out: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)
* Option: `nullValues`: Set if null values should not be returned as empty strings.


## read-json-object

*Reads in a JSON file and splits it at the end of the root object / array. Preferably used in conjunction with [decode-json](#decode-json)* 

* Implementation: [org.swissbib.linked.json.JsonObjectReader](src/main/java/org/swissbib/linked/json/JsonObjectReader.java)
* In: `java.lang.Reader`
* Out: `java.lang.String`

Example: [libadmin entries as Ntriples](https://github.com/sschuepbach/metafacture-examples/tree/master/Swissbib-Extensions/Libadmin-Ntriples)