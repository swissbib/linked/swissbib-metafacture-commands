# Extensions for [`metafacture-csv`](https://github.com/metafacture/metafacture-core/tree/master/metafacture-csv)

Provides commands for handling CSV data (i.e. data with comma-separated values).

* [write-csv](#write-csv): Serialises data as CSV file with optional header.

## write-csv

*Serialises data as CSV file with optional header*

* Implementation: [org.swissbib.linked.csv.ContinuousCsvWriter](src/main/java/org/swissbib/linked/csv/ContinuousCsvWriter.java)
* In: `java.lang.String`
* Out: `java.lang.Void`
* Options:
    * compression: Sets the compression mode
    * continuousFile: Boolean. If set to true, the header is only written to the first file.
    * encoding: Sets the encoding used by the underlying writer
    * filenamePostfix: By default the filename consists of a zero-filled sequential number with six digits. Sets a postfix for this number.
    * filenamePrefix: By default the filename consists of a zero-filled sequential number with six digits. Sets a prefix for this number.
    * filetype: File ending
    * footer: Sets the footer which is output after the last object
    * header: Sets the header which is output before the first object
    * linesPerFile: Number of lines written to one file
    * path: Path to directory with CSV files
    * separator: Sets the separator which is output between objects

Examples: [Workflow which queries the Swissbib SRU interface and filters, transforms and dumps the results to a CSV file](https://github.com/sschuepbach/metafacture-examples/tree/master/Swissbib-Extensions/Swissbib-SRU)
