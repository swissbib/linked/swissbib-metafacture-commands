package org.swissbib.linked.elasticsearch;

import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.message.BasicHeader;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Creates a transport client for Elasticsearch. Because we don't need several transport clients connecting to the
 * cluster, we have to make sure that only one instance will be created in the workflow.
 *
 * @author Sebastian Schüpbach
 * @version 0.1
 * <p>
 * Created on 13.01.16
 */
public class RestHighLevelClientSingleton {

    private final static Logger LOG = LoggerFactory.getLogger(RestHighLevelClientSingleton.class);
    private static RestHighLevelClient esClient;

    // Prevents directly instantiating class.
    private RestHighLevelClientSingleton() {
    }

    /**
     * Checks if an Elasticsearch transport client is already instantiated, and returns either the already or the newly
     * instantiated client.
     *
     * @param nodes       Nodes of the Elasticsearch cluster
     * @param clustername Name of the Elasticsearch cluster
     * @return The instantiated Elasticsearch transport client
     */
    static synchronized RestHighLevelClient getEsClient(String[] nodes, String clustername) {
        if (esClient == null) {
            LOG.info("Connecting to Elasticsearch cluster {}", clustername);
            List<HttpHost> httpHosts = new ArrayList<>();
            for (String elem : nodes) {
                String[] node = elem.split(":");
                HttpHost httpHost = new HttpHost(node[0], Integer.parseInt(node[1]), "http");
                httpHosts.add(httpHost);
            }
            Header[] defaultHeaders = new Header[]{
                    new BasicHeader("cluster.name", clustername)
            };
            RestClientBuilder builder = RestClient.builder(httpHosts.toArray(new HttpHost[0]));
            builder.setDefaultHeaders(defaultHeaders);
            esClient = new RestHighLevelClient(
                    builder
            );
        }
        return esClient;
    }

    /**
     * Calling of inherited clone() method throws an error
     *
     * @return An error
     * @throws CloneNotSupportedException Thrown if clone() is called
     */
    @SuppressWarnings("CloneDoesntCallSuperClone")
    @Override
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    /**
     * Shuts down Elasticsearch transport client
     *
     * @throws Throwable Exception
     */
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        LOG.info("Shutting down Elasticsearch transport client");
        esClient.close();
    }
}
