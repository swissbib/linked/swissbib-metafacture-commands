package org.swissbib.linked.elasticsearch;

import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.metafacture.framework.StreamReceiver;
import org.metafacture.framework.annotations.Description;
import org.metafacture.framework.annotations.In;
import org.metafacture.framework.annotations.Out;
import org.metafacture.framework.helpers.DefaultStreamPipe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @author Sebastian Schüpbach
 * @version 0.1
 * <p>
 * Created on 24.03.16
 */
@Description("Filters out records which already exists in Elasticsearch index.")
@In(StreamReceiver.class)
@Out(StreamReceiver.class)
public class ESItemEraser extends DefaultStreamPipe<StreamReceiver> {

    private final static Logger LOG = LoggerFactory.getLogger(ESBulkIndexer.class);

    private RestHighLevelClient esClient;
    private String esClustername;
    private String[] esNodes;
    private String esIndex;


    @SuppressWarnings("unused")
    public void setEsClustername(final String esClustername) {
        this.esClustername = esClustername;
        LOG.debug("Settings - Set cluster name for Elasticsearch: {}", esClustername);
    }


    @SuppressWarnings("unused")
    public void setEsNodes(final String esNode) {
        this.esNodes = esNode.split("#");
        LOG.debug("Settings - Set addresses of Elasticsearch nodes: {} (# is a delimiter)", esNode);
    }


    @SuppressWarnings("unused")
    public void setEsIndex(String esIndex) {
        this.esIndex = esIndex;
    }

    @Override
    public void startRecord(String identifier) {
        try {
            eraseItems("https://data.swissbib.ch/resource/" + identifier);
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
        getReceiver().startRecord(identifier);
    }


    @Override
    public void endRecord() {
        getReceiver().endRecord();
    }


    @Override
    public void startEntity(String name) {
        getReceiver().startEntity(name);
    }


    @Override
    public void endEntity() {
        getReceiver().endEntity();
    }


    @Override
    public void literal(String name, String value) {
        getReceiver().literal(name, value);
    }


    @Override
    protected void onCloseStream() {
        try {
            esClient.close();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }


    private void eraseItems(String resId) throws IOException {
        if (esClient == null)
            esClient = RestHighLevelClientSingleton.getEsClient(esNodes, esClustername);
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder()
                .query(QueryBuilders.termQuery("bf:holdingFor", resId));
        SearchHits sh = esClient
                .search(new SearchRequest(esIndex)
                                .searchType(SearchType.DFS_QUERY_THEN_FETCH)
                                .source(sourceBuilder),
                        RequestOptions.DEFAULT)
                .getHits();

        for (SearchHit h : sh) {
            esClient.delete(new DeleteRequest(esIndex, h.getId()), RequestOptions.DEFAULT);
            LOG.debug("Document {} deleted.", h.getId());
        }
    }
}
