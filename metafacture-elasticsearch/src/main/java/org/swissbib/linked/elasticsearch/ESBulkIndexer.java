package org.swissbib.linked.elasticsearch;

import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.bytes.BytesArray;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.metafacture.framework.MetafactureException;
import org.metafacture.framework.annotations.Description;
import org.metafacture.framework.annotations.In;
import org.metafacture.framework.annotations.Out;
import org.metafacture.io.ConfigurableObjectWriter;
import org.metafacture.io.FileCompression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;


/**
 * Writes Elasticsearch Bulk API compliant strings to Elasticsearch Index
 *
 * @author Sebastian Schüpbach, project swissbib, Basel
 */
@Description("Outputs an Elasticsearch Bulk API compliant file.")
@In(Object.class)
@Out(Void.class)
public class ESBulkIndexer<T> implements ConfigurableObjectWriter<T> {

    private static final String SET_COMPRESSION_ERROR = "Cannot compress Triple store";
    private final static Logger LOG = LoggerFactory.getLogger(ESBulkIndexer.class);
    private String[] esNodes = {"localhost:9300"};
    private String esClustername = "linked-swissbib";
    private int recordsPerUpload = 2000;

    private RestHighLevelClient esClient;
    private BulkProcessor bulkProcessor;


    @SuppressWarnings("unused")
    public void setEsClustername(final String esClustername) {
        this.esClustername = esClustername;
        LOG.debug("Settings - Set cluster name for Elasticsearch: {}", esClustername);
    }


    @SuppressWarnings("unused")
    public void setRecordsPerUpload(final int recordsPerUpload) {
        this.recordsPerUpload = recordsPerUpload;
        LOG.debug("Settings - Set number of records per bulk upload: {}", recordsPerUpload);
    }


    @SuppressWarnings("unused")
    public void setEsNodes(final String esNode) {
        this.esNodes = esNode.split("#");
        LOG.debug("Settings - Set addresses of Elasticsearch nodes: {} (# is a delimiter)", esNode);
    }


    @Override
    public String getEncoding() {
        return Charset.defaultCharset().toString();
    }


    @Override
    public void setEncoding(String encoding) {
        throw new UnsupportedOperationException("Cannot change encoding of Search engine");
    }


    @Override
    public FileCompression getCompression() {
        return FileCompression.NONE;
    }

    @Override
    public void setCompression(FileCompression compression) {
        throw new UnsupportedOperationException(SET_COMPRESSION_ERROR);
    }

    @Override
    public void setCompression(String compression) {
        throw new UnsupportedOperationException(SET_COMPRESSION_ERROR);
    }

    @Override
    public String getHeader() {
        return DEFAULT_HEADER;
    }

    /**
     * Sets the header which is output before the first object.
     *
     * @param header new header string
     */
    @Override
    public void setHeader(String header) {

    }


    @Override
    public String getFooter() {
        return DEFAULT_FOOTER;
    }

    /**
     * Sets the footer which is output after the last object.
     *
     * @param footer new footer string
     */
    @Override
    public void setFooter(String footer) {

    }


    @Override
    public String getSeparator() {
        return DEFAULT_SEPARATOR;
    }

    /**
     * Sets the separator which is output between objects.
     *
     * @param separator new separator string
     */
    @Override
    public void setSeparator(String separator) {

    }


    public void process(T obj) {
        LOG.trace("Adding record to bulk processor");
        esClient = RestHighLevelClientSingleton.getEsClient(esNodes, this.esClustername);
        if (bulkProcessor == null) createTransportClient();

        if (!obj.equals("{}\n")) {
            BytesArray ba = new BytesArray((String) obj);
            try {
                this.bulkProcessor.add(ba, null, null, XContentType.JSON);
            } catch (Exception e) {
                LOG.error(e.getMessage());
            }
        }
    }

    private void createTransportClient() {

        BulkProcessor.Listener listener = new BulkProcessor.Listener() {
            @Override
            public void beforeBulk(long executionId, BulkRequest request) {
                LOG.debug("Bulk requests to be processed: {}", request.numberOfActions());
            }

            @Override
            public void afterBulk(long executionId, BulkRequest request,
                                  BulkResponse response) {
                int successful = 0;
                int failures = 0;
                if (response.hasFailures()) {
                    for (BulkItemResponse item : response.getItems()) {
                        if (item.isFailed()) {
                            LOG.error(item.getFailureMessage());
                            failures += 1;
                        } else {
                            successful += 1;
                        }
                    }
                    LOG.error("Failed to index {} requests.", failures);
                } else {
                    successful = request.numberOfActions();
                }
                LOG.info("Indexed {} requests.", successful);
            }

            @Override
            public void afterBulk(long executionId, BulkRequest request,
                                  Throwable failure) {
                LOG.error("Some errors were reported: {}", failure.getMessage());
            }

        };



        bulkProcessor = BulkProcessor.builder(
                (request, bulkListener) ->
                        esClient.bulkAsync(request, RequestOptions.DEFAULT, bulkListener),
                listener)
                .setBulkActions(this.recordsPerUpload)
                .setBulkSize(new ByteSizeValue(1L, ByteSizeUnit.MB))
                .setConcurrentRequests(0)
                .setFlushInterval(TimeValue.timeValueSeconds(10L))
                .build();

    }

    @Override
    public void resetStream() {
        this.bulkProcessor.flush();
    }


    @Override
    public void closeStream() {
        LOG.info("Prepare closing bulk processor.");
        try {
            this.bulkProcessor.awaitClose(60L, TimeUnit.SECONDS);
            this.esClient.close();
            LOG.info("Closed bulk processor.");
        } catch (IOException | InterruptedException ioExcep) {
            LOG.error("Error closing the RestHighLevelClient");
            throw new MetafactureException(ioExcep);
        }
    }

}
