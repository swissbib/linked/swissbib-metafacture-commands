# Extensions for [`metafacture-elasticsearch`](https://github.com/metafacture/metafacture-core/tree/master/metafacture-elasticsearch)

Provides commands for handling Elasticsearch requests:

* [itemerase-es](#itemerase-ex): Deletes items which belong to a certain bibliographicResource
* [lookup-es](#lookup-es): Filters out records whose identifier already exists in an Elasticsearch index
* [update-es-id](#update-es-id): Identifies partially modified documents by comparing them to an Elasticsearch index.
* [index-esbulk](#index-esbulk): Uses the bulk mechanisms of Elasticsearch to index records


## index-esbulk

*Indexes records in Elasticsearch.*

* Implementation: [org.swissbib.linked.elasticsearch.ESBulkIndexer](src/main/java/org/swissbib/linked/elasticsearch/ESBulkIndexer.java)
* In: `java.lang.Object`
* Out: `java.lang.Void`
* Options:
    * esClustername: Elasticsearch cluster name
    * recordsPerUpload: Number of records per single bulk upload
    * esNodes: Elasticsearch nodes. Nodes are separated by #

Example: [linked-swissbib "Baseline"](https://github.com/sschuepbach/metafacture-examples/tree/master/Swissbib-Extensions/Linked-Swissbib-Baseline)


## itemerase-es

*Deletes items which belong to a certain bibliographicResource. Recommended for internal use only. Intended to use with the tracking framework of linked-swissbib*

* Implementation: [org.swissbib.linked.elasticsearch.ESItemErase](src/main/java/org/swissbib/linked/elasticsearch/ESItemErase.java)
* In: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)
* Out: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)
* Options:
    * esClustername: Elasticsearch cluster name
    * esNodes: Elasticsearch nodes. Nodes are separated by #
    * esIndex: Elasticsearch index


## lookup-es

*Filters out records whose identifier already exists in an Elasticsearch index. Intended to use with the tracking framework of linked-swissbib.*

* Implementation: [org.swissbib.linked.elasticsearch.ESLookup](src/main/java/org/swissbib/linked/elasticsearch/ESLookup.java)
* In: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)
* Out: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)
* Options:
    * esClustername: Elasticsearch cluster name
    * esNodes: Elasticsearch nodes. Nodes are separated by #
    * esIndex: Elasticsearch index


## update-es-id

*Identifies partially modified documents by comparing them to an Elasticsearch index. Is tailored to the so-called baseline workflow of linked-swissbib, so it's probably useless for other purposes*

* Implementation: [org.swissbib.linked.elasticsearch.ESIdUpdate](src/main/java/org/swissbib/linked/elasticsearch/ESIdUpdate.java)
* In: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)
* Out: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)
* Options:
    * esClustername: Elasticsearch cluster name
    * esNodes: Elasticsearch nodes. Nodes are separated by #
    * esIndex: Elasticsearch index
    * matchingFields: Fields which should be matched. # is delimiter.
    * sThreshold: Matching threshold
    * refPath: Name of person / organisation field in bibliographicResoruce
    * uriPrefix: Prefix for identifier (e.g. http://data.swissbib.ch/person/)
    * graphDbDir: Path to Neo4j database
