package org.swissbib.linked.metamorph.functions;


import org.metafacture.metamorph.api.helpers.AbstractSimpleStatelessFunction;
import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@FunctionalInterface
interface CreateLink {

    String create(HashMap<String,Optional<String>> data);
}




public final class ItemLink extends AbstractSimpleStatelessFunction {

    private HashMap<String, Pattern> systemNumberPattern;

    private final static HashMap<String, CreateLink> processLink;

    static {
        processLink = new HashMap<>();
        processLink.put("NEBIS", data -> {
            if (data.containsKey("035Id") && data.get("035Id").isPresent()) {
                String template = "https://recherche.nebis.ch/primo-explore/fulldisplay?docid=ebi01_prod{bib-system-number}&context=L&vid=NEBIS&search_scope=default_scope&tab=default_tab";
                return  template.replace("{bib-system-number}", data.get("035Id").get());
            }
            return "";
        });
        processLink.put("IDSBB", data -> {
            if (data.containsKey("swissbibId") && data.get("swissbibId").isPresent() &&
                    data.containsKey("institutionCode") && data.get("institutionCode").isPresent()) {
                String template = "https://baselbern.swissbib.ch/Record/{id}?expandlib={sub-library-code}#holding-institution-IDSBB-{sub-library-code}";
                return  template.replace("{id}", data.get("swissbibId").get()).replace("{sub-library-code}", data.get("institutionCode").get());
            }
            return "";
        });
        processLink.put("RERO", data -> {

            String template = "http://data.rero.ch/01-{bib-system-number}/html?view={RERO-network}_V1";
            String institutionCode =  data.get("institutionCode").orElse("");
            if (institutionCode.length() == 0 | institutionCode.length() < 4) return "";
            String rero_network_code = institutionCode.substring(2,4);

            String rero_network = "";

            switch (rero_network_code) {
                case "01": rero_network = "FR"; break;
                case "11": rero_network = "VS"; break;
                case "31": rero_network = "NJ"; break;
                case "61": rero_network = "GE"; break;
                case "71": rero_network = "IF"; break;
                default: rero_network = "";
            }

            String bib_sysnumber =  data.get("035Id").orElse("");
            if (bib_sysnumber.length() == 0 ) return "";
            return template.replace("{bib-system-number}", bib_sysnumber).replace("{RERO-network}",rero_network);
        });
        processLink.put("SGBN", data -> {

            if (data.containsKey("035Id") && data.get("035Id").isPresent()) {
                String template = "https://sgbn-primo.hosted.exlibrisgroup.com/primo-explore/fulldisplay?docid=41STGKBG_ALEPH{bib-system-number}&context=L&vid=41STGKBG_VU1&search_scope=41STGKBG-Lokal-PC&tab=default_tab&lang=de_DE";
                return template.replace("{bib-system-number}", data.get("035Id").get());
            }

            return "";
        });
        processLink.put("IDSSG", data -> {



            if (data.containsKey("035Id") && data.get("035Id").isPresent() &&
                    data.containsKey("institutionCode") && data.get("institutionCode").isPresent()) {

                String instCode = data.get("institutionCode").get();
                String template = null;
                String backlink = null;
                switch (instCode) {
                    case "HPHS":
                    case "HPHG":
                    case "HPHRS":
                    case "HPHRM":
                    case "HRDZJ":
                    case "HRDZS":
                    case "HRDZW":
                    case "HRPMA":
                    case "HPHOL":
                        template = "http://aleph.unisg.ch/F?func=item-global&doc_library=HSB01&doc_number={bib-system-number}&sub_library={aleph-sublibrary-code}&local_base=ph";
                        backlink = template.replace("{bib-system-number}", data.get("035Id").get()).replace("{aleph-sublibrary-code}",instCode);
                        break;
                    case "HFHS":
                        template = "http://aleph.unisg.ch/F?func=item-global&doc_library=HSB01&doc_number={bib-system-number}&sub_library={aleph-sublibrary-code}&local_base=fhs";
                        backlink = template.replace("{bib-system-number}", data.get("035Id").get()).replace("{aleph-sublibrary-code}",instCode);
                        break;
                    default:
                        template = "https://aleph.unisg.ch/php/bib_holdings.php?docnr={bib-system-number}";
                        backlink = template.replace("{bib-system-number}", data.get("035Id").get());
                }

                return backlink;
            }

            return "";
        });


        processLink.put("ABN", data -> {
            if (data.containsKey("035Id") && data.get("035Id").isPresent()) {
                String template = "https://abn-primo.hosted.exlibrisgroup.com/primo-explore/fulldisplay?docid=41ABN_ALEPH_DS{bib-system-number}&context=L&vid=41ABN_VU1&search_scope=ABN51&tab=default_tab";
                return template.replace("{bib-system-number}", data.get("035Id").get());
            }
            return "";

        });

        processLink.put("IDSLU", data -> {
            if (data.containsKey("035Id") && data.get("035Id").isPresent()) {
                String template = "https://iluplus.hosted.exlibrisgroup.com/41ZBL:institution_scope:41ZBL_Aleph{bib-system-number}";
                return template.replace("{bib-system-number}", data.get("035Id").get());
            }
            return "";
        });
        processLink.put("BGR", data -> {
            if (data.containsKey("035Id") && data.get("035Id").isPresent() &&
                    data.containsKey("institutionCode") && data.get("institutionCode").isPresent()) {
                String template = "http://aleph.gr.ch/F?func=item-global&doc_library=BGR01&doc_number={bib-system-number}&sub_library={aleph-sublibrary-code}";
                return template.replace("{bib-system-number}", data.get("035Id").get()).replace("{aleph-sublibrary-code}", data.get("institutionCode").get());
            }
            return "";
        });

        processLink.put("SBT", data -> {
            if (data.containsKey("035Id") && data.get("035Id").isPresent() &&
                    data.containsKey("institutionCode") && data.get("institutionCode").isPresent()) {
                String template = "http://aleph.sbt.ti.ch/F?func=item-global&doc_library=SBT01&doc_number={bib-system-number}&sub_library={aleph-sublibrary-code}";
                return template.replace("{bib-system-number}", data.get("035Id").get()).replace("{aleph-sublibrary-code}", data.get("institutionCode").get());
            }
            return "";
        });


        processLink.put("SNL", data -> {

            if (data.containsKey("035Id") && data.get("035Id").isPresent()) {
                String template = "https://nb-helveticat.primo.exlibrisgroup.com/discovery/fulldisplay?docid=alma{bib-system-number}&context=L&vid=41SNL_51_INST:helveticat&search_scope=MyInstitution&tab=LibraryCatalog";
                return template.replace("{bib-system-number}", data.get("035Id").get());
            }
            return "";
        });
        processLink.put("ALEX", data -> {
            if (data.containsKey("035Id") && data.get("035Id").isPresent()) {
                String template = "https://www.alexandria.ch/primo-explore/search?query=any,contains,{bib-system-number}&sortby=rank&vid=ALEX";
                return template.replace("{bib-system-number}", data.get("035Id").get());
            }
            return "";
        });
        processLink.put("CCSA", data -> {

            if (data.containsKey("035Id") && data.get("035Id").isPresent()) {
                String template = "https://nb-posters.primo.exlibrisgroup.com/discovery/fulldisplay?docid=alma{bib-system-number}&context=L&vid=41SNL_53_INST:posters&search_scope=MyInstitution&tab=LibraryCatalog";
                return template.replace("{bib-system-number}", data.get("035Id").get());
            }
            return "";
        });
        processLink.put("LIBIB", data -> {
            if (data.containsKey("035Id") && data.get("035Id").isPresent() &&
                    data.containsKey("institutionCode") && data.get("institutionCode").isPresent()) {
                String template = "http://aleph.lbfl.li/F?func=item-global&doc_library=LLB01&doc_number={bib-system-number}&sub_library={aleph-sublibrary-code}";
                return template.replace("{bib-system-number}", data.get("035Id").get()).replace("{aleph-sublibrary-code}", data.get("institutionCode").get());
            }
            return "";
        });
        processLink.put("BISCH", data -> {

            if (data.containsKey("035Id") && data.get("035Id").isPresent()) {
                String template = "https://webopac.bibliotheken-schaffhausen.ch/TouchPoint_touchpoint/perma.do?q=0%3D%22{bib-system-number}%22+IN+%5B2%5D&v=extern&l=de";
                return template.replace("{bib-system-number}", data.get("035Id").get());
            }
            return "";
        });
        processLink.put("CEO", data -> {

            if (data.containsKey("035Id") && data.get("035Id").isPresent()) {
                String template = "https://library.olympic.org/Default/doc/SYRACUSE/{bib-system-number}/";
                return template.replace("{bib-system-number}", data.get("035Id").get());
            }
            return "";
        });
        processLink.put("HEMU", data -> {

            if (data.containsKey("035Id") && data.get("035Id").isPresent()) {
                String template = "http://opacbiblio.hemu-cl.ch/cgi-bin/koha/opac-detail.pl?biblionumber={bib-system-number}";
                return template.replace("{bib-system-number}", data.get("035Id").get());
            }
            return "";
        });

        processLink.put("KBTG", data -> {

            if (data.containsKey("035Id") && data.get("035Id").isPresent()) {
                String template = "http://netbiblio.tg.ch/kbtg/search/notice?noticeID={bib-system-number}";
                return template.replace("{bib-system-number}", data.get("035Id").get());
            }
            return "";
        });
        processLink.put("VAUD", data -> {
            if (data.containsKey("035Id") && data.get("035Id").isPresent()) {
                String template = "https://renouvaud.hosted.exlibrisgroup.com/primo-explore/search?vid=41BCULIB_VU2&search_scope=41BCULIB_ALMA_ALL&query=any,contains,{bib-system-number}&sortby=rank";
                return template.replace("{bib-system-number}", data.get("035Id").get());
            }
            return "";
        });
        processLink.put("VAUDS", data -> {

            if (data.containsKey("035Id") && data.get("035Id").isPresent()) {
                String template = "https://renouvaud.hosted.exlibrisgroup.com/primo-explore/search?vid=41BCULIB_VU2&search_scope=41BCULIB_ALMA_ALL&query=any,contains,{bib-system-number}&sortby=rank";
                return template.replace("{bib-system-number}", data.get("035Id").get());
            }
            return "";
        });

        processLink.put("CHARCH", data -> {
            if (data.containsKey("035Id") && data.get("035Id").isPresent()) {
                String template = "https://www.helveticarchives.ch/detail.aspx?ID={bib-system-number}";
                return template.replace("{bib-system-number}", data.get("035Id").get());
            }
            return "";
        });

        processLink.put("DEFAULT", data -> "");

        processLink.put("SERVAL", ItemLink::checkRepositories);

        processLink.put("ZORA", ItemLink::checkRepositories);

        processLink.put("ETHRESEARCH", ItemLink::checkRepositories);

        processLink.put("BORIS", ItemLink::checkRepositories);

        processLink.put("ALEXREPO", ItemLink::checkRepositories);

        processLink.put("EDOC", ItemLink::checkRepositories);

        processLink.put("ECOD", ItemLink::checkRepositories);
    }

    private static String checkRepositories(HashMap<String, Optional<String>> data) {
        if (data.containsKey("949u") && data.get("949u").isPresent()) {

            return data.get("949u").get();
        }
        return "";
    }

    private static String replaceBibSysWith035id(HashMap<String, Optional<String>> data) {
        if (data.containsKey("949u") && data.get("949u").isPresent()) {

            return data.get("949u").get();
        }
        return "";
    }



    public ItemLink() throws IOException {

        this.initializeNetworkRegex();
    }

    @Override
    protected String process(String value) {

        HashMap<String,Optional<String>> urlParts = this.checkValidity(value);
        if (urlParts.size() == 0)   return "";

        //return processLink.get(metamorphParts[0]).create(urlParts);
        return processLink.get(urlParts.getOrDefault("source",Optional.of("DEFAULT")).get()).create(urlParts);

    }


    //private HashMap<String, Optional<String>> checkValidity (String [] valueParts)
    private HashMap<String, Optional<String>> checkValidity (String metamorphValues)
    {

        String[] valueParts =  metamorphValues.split("##");

        //in case there is no 949u in metamorphValues the last two characters of metamorphValues are ## and the split
        //method return 6 parts otherwise 7 parts

        //without 949u: CEO##CEO##CEO####575177284##(CEO)209131##
        //with 949u: ZORA##ZORA##ZORA##oai:www.zora.uzh.ch:173305##577995057##(ZORA)oai:www.zora.uzh.ch:173305!!(ZORA)oai:www.zora.uzh.ch:177578##https://www.zora.uzh.ch/id/eprint/173305

        if (valueParts.length != 7 && valueParts.length != 6) return new HashMap<>();
        if (!this.systemNumberPattern.containsKey(valueParts[0])) return new HashMap<>();
        Pattern p = this.systemNumberPattern.get(valueParts[0]);
        Matcher m = p.matcher(valueParts[5]);
        if (!m.find()) return new HashMap<>();

        HashMap<String, Optional<String>> urlParts = new HashMap<>();

        urlParts.put("source",Optional.of(valueParts[0]));

        urlParts.put("035Id",Optional.of(m.group(1)));

        urlParts.put("institutionCode",valueParts[2].length() > 0 ? Optional.of(valueParts[2]) :
                valueParts[1].length() > 0
                ? Optional.of(valueParts[1]) : Optional.empty());
        urlParts.put("bibsysnumber",valueParts[2].length() > 0 ? Optional.of(valueParts[2]) :
                Optional.empty());

        urlParts.put("swissbibId",valueParts[4].length() > 0 ? Optional.of(valueParts[4]) :
                Optional.empty());

        urlParts.put("035Ids",valueParts[5].length() > 0 ? Optional.of(valueParts[5]) :
                Optional.empty());

        urlParts.put("949u",valueParts.length == 7 && valueParts[6].length() > 0 ? Optional.of(valueParts[6]) :
                Optional.empty());

        return urlParts;

    }

    private void initializeNetworkRegex() {
        //(RERO)R004689410!!(SGBN)000187319!!(NEBIS)000262918!!(IDSBB)000217483
        //we need (!!|$) to catch a pattern (IDSBB)000217483 at the end of the string (without !! as delimiter)
        Pattern p = Pattern.compile("\\(IDSBB\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern = new HashMap<>();
        this.systemNumberPattern.put("IDSBB", p);
        p = Pattern.compile("\\(RERO\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("RERO", p);

        p = Pattern.compile("\\(SGBN\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("SGBN", p);

        p = Pattern.compile("\\(NEBIS\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("NEBIS", p);

        p = Pattern.compile("\\(IDSSG\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("IDSSG", p);

        p = Pattern.compile("\\(IDSSG2\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("IDSSG2", p);

        p = Pattern.compile("\\(IDSLU\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("IDSLU", p);

        p = Pattern.compile("\\(BGR\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("BGR", p);

        p = Pattern.compile("\\(SBT\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("SBT", p);

        p = Pattern.compile("\\(ABN\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("ABN", p);

        p = Pattern.compile("\\(SNL\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("SNL", p);

        p = Pattern.compile("\\(ALEX\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("ALEX", p);

        p = Pattern.compile("\\(CCSA\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("CCSA", p);

        p = Pattern.compile("\\(CHARCH\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("CHARCH", p);

        p = Pattern.compile("\\(LIBIB\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("LIBIB", p);

        p = Pattern.compile("\\(HEMU\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("HEMU", p);

        p = Pattern.compile("\\(BISCH\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("BISCH", p);

        p = Pattern.compile("\\(IDSSGPH\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("IDSSGPH", p);

        p = Pattern.compile("\\(CEO\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("CEO", p);

        p = Pattern.compile("\\(KBTG\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("KBTG", p);

        p = Pattern.compile("\\(VAUD\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("VAUD", p);

        p = Pattern.compile("\\(VAUDS\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("VAUDS", p);

        p = Pattern.compile("\\(SERVAL\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("SERVAL", p);

        p = Pattern.compile("\\(ZORA\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("ZORA", p);

        p = Pattern.compile("\\(ETHRESEARCH\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("ETHRESEARCH", p);

        p = Pattern.compile("\\(BORIS\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("BORIS", p);

        p = Pattern.compile("\\(ALEXREPO\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("ALEXREPO", p);

        p = Pattern.compile("\\(EDOC\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("EDOC", p);

        p = Pattern.compile("\\(ECOD\\)(.*?)(!!|$)",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
        this.systemNumberPattern.put("ECOD", p);


    }

}
