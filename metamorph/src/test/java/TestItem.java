import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.metafacture.biblio.marc21.MarcXmlHandler;
import org.metafacture.formeta.FormetaEncoder;
import org.metafacture.formeta.formatter.FormatterStyle;
import org.metafacture.io.ConfigurableObjectWriter;
import org.metafacture.io.ObjectStdoutWriter;
import org.metafacture.mangling.RecordIdChanger;
import org.metafacture.metamorph.Filter;
import org.metafacture.metamorph.Metamorph;
import org.metafacture.strings.StringReader;
import org.metafacture.xml.XmlDecoder;
import org.swissbib.linked.mangling.EntitySplitter;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import org.swissbib.linked.io.StringWriter;

public class TestItem {

    private static StringReader stringReader;
    private static HashMap<String, String> output = new HashMap<>();

    @BeforeAll
    public static void buildPipe() {

        final StringReader sReader = new StringReader();
        final XmlDecoder xmlDecoder = new XmlDecoder();
        final MarcXmlHandler marcXmlHandler = new MarcXmlHandler();
        final Filter filter = new Filter("morphs/245aFilter.xml");
        final Metamorph metamorph = new Metamorph("morphs/itemMorph.xml");
        final RecordIdChanger recordIdChanger = new RecordIdChanger();

        sReader.setReceiver(xmlDecoder)
                .setReceiver(marcXmlHandler)
                .setReceiver(filter)
                .setReceiver(metamorph);

        final ObjectStdoutWriter<String> writer = new ObjectStdoutWriter<>();

        final ObjectStdoutWriter<String> w = new ObjectStdoutWriter<>();


        final FormetaEncoder formetaEncoder = new FormetaEncoder();
        formetaEncoder.setStyle(FormatterStyle.VERBOSE);

        final EntitySplitter entitySplitter = new EntitySplitter();
        metamorph.setReceiver(entitySplitter)
                .setReceiver(recordIdChanger)
                .setReceiver(formetaEncoder)
                .setReceiver(writer);

        stringReader = sReader;

        initializeOutput();

    }


    private <T> StringReader buildSingletonPipe(ConfigurableObjectWriter<String> cw) {

        final StringReader sReader = new StringReader();
        final XmlDecoder xmlDecoder = new XmlDecoder();
        final MarcXmlHandler marcXmlHandler = new MarcXmlHandler();
        final Filter filter = new Filter("morphs/245aFilter.xml");
        final Metamorph metamorph = new Metamorph("morphs/itemMorph.xml");
        final RecordIdChanger recordIdChanger = new RecordIdChanger();

        sReader.setReceiver(xmlDecoder)
                .setReceiver(marcXmlHandler)
                .setReceiver(filter)
                .setReceiver(metamorph);

        //final ObjectStdoutWriter<String> writer = new ObjectStdoutWriter<>();

        //final ObjectStdoutWriter<String> w = new ObjectStdoutWriter<>();


        final FormetaEncoder formetaEncoder = new FormetaEncoder();
        formetaEncoder.setStyle(FormatterStyle.VERBOSE);

        final EntitySplitter entitySplitter = new EntitySplitter();
        metamorph.setReceiver(entitySplitter)
                .setReceiver(recordIdChanger)
                .setReceiver(formetaEncoder)
                .setReceiver(cw);

        return sReader;
    }


    @Test
    public void testIDSSG() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemIDSSG.xml")));
            //buildSingletonPipe(new StringWriter<>()).process(s);
            //stringReader.process(s);
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("idssg"), sw.getAndReset(), "wrong idssg output");
        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemIDSSG.xml not found");
        }

    }

    @Test
    public void testRERO() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemRERO.xml")));
            //stringReader.process(s);
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("rero"), sw.getAndReset(), "wrong rero output");

        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemRERO.xml not found");
        }

    }


    @Test
    public void testIDSBB() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemIDSBB.xml")));
            //stringReader.process(s);
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("idsbb"), sw.getAndReset(), "wrong idsbb output");
        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemBB.xml not found");
        }
    }

    @Test
    public void testSGBN() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemSGBN.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("sgbn"), sw.getAndReset(), "wrong sgbn output");

        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemSGBN.xml not found");
        }
    }

    @Test
    public void testABN() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemABN.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("abn"), sw.getAndReset(), "wrong Aarau output");
        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemABN.xml not found");
        }
    }

    @Test
    public void testLU() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemLU.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("ilu"), sw.getAndReset(), "wrong Lucerne output");
        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemLU.xml not found");
        }
    }

    @Test
    public void testBGR() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemBGR.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);

            assertEquals(output.get("bgr"), sw.getAndReset(), "wrong Graubünden output");


        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemBGR.xml not found");
        }
    }

    @Test
    public void testSNLSeries() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemSNL.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            //for series no items are generated (949 tags are missing)
            assertEquals("", sw.getAndReset(), "wrong SNL Series output");


        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemSNL.xml  (for series) not found");
        }
    }

    @Test
    public void testSNL() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemSNL1.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("snl"), sw.getAndReset(), "wrong SNL output");

        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemSNL1.xml  (for books) not found");
        }
    }

    @Test
    public void testAlex() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemAlex.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("alex"), sw.getAndReset(), "wrong Alexandria output");

        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemAlex.xml not found");
        }
    }

    @Test
    public void testPoster() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemPOSTER.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("poster"), sw.getAndReset(), "wrong Poster output");

        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemPOSTER.xml not found");
        }
    }

    @Test
    public void testLIBIB() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemLIBIB.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("libib"), sw.getAndReset(), "wrong Lichtenstein output");

        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemLIBIB.xml not found");
        }
    }

    @Test
    public void testSchaffhausen() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemBISCH.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("bisch"), sw.getAndReset(), "wrong Schaffhausen output");

        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemBISCH.xml not found");
        }
    }

    @Test
    public void testMusikSchuleLausanne() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemHEMU.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("hemu"), sw.getAndReset(), "wrong hemu output");


        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemHEMU.xml not found");
        }
    }

    @Test
    public void testRenouvaud() {
        //are there differences for backlinks between vaud and vauds (schools??)

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemVAUD.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("vaud"), sw.getAndReset(), "wrong vaud output");

        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemVAUD.xml not found");
        }
    }

    @Test
    public void testKbThurgau() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemKBTG.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("kbthurgau"), sw.getAndReset(), "wrong kb thurgau output");

        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemKBTG.xml not found");
        }
    }

    @Test
    public void testIOC() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemCEO.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("ioc"), sw.getAndReset(), "wrong IOC output");

        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemCEO.xml not found");
        }
    }

    @Test
    public void testidssgPH() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemIDSSGPH.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("idssgph"), sw.getAndReset(), "wrong idssg Fachhochschule output");

        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemIDSSGPH.xml not found");
        }
    }

    @Test
    public void testidssgFH() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemIDSSGFH.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("idssgfh"), sw.getAndReset(), "wrong idssg Fachhochschule output");
        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemIDSSGFH.xml not found");
        }
    }
    @Test
    public void testHelveticArchive() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemHA.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("ha"), sw.getAndReset(), "wrong helvetic archive output");

        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemHA.xml not found");
        }
    }

    @Test
    public void testSBT() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemSBT.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("sbt"), sw.getAndReset(), "wrong sbt output");
        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemSBT.xml not found");
        }


    }

    @Test
    public void testSERVAL() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemSERVAL.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("serval"),sw.getAndReset(),"wrong serval output");

        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemSERVAL.xml not found");
        }
    }

    @Test
    public void testZORA() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemZORA.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("zora"),sw.getAndReset(),"wrong zora output");

        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemZORA.xml not found");
        }
    }

    @Test
    public void testBORIS() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemBoris.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("boris"),sw.getAndReset(),"wrong boris output");

        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemBORIS.xml not found");
        }
    }

    @Test
    public void testAlexrepo() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemAlexrepo.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("alexrepo"),sw.getAndReset());

        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemAlexrepo.xml not found");
        }
    }

    @Test
    public void testEdoc() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemEdoc.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("edoc"),sw.getAndReset());
        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemEdoc.xml not found");
        }
    }

    @Test
    public void testEcod() {

        try {
            String s = new String(Files.readAllBytes(Paths.get("src/test/resources/data/item/itemEcod.xml")));
            StringWriter<String> sw = new StringWriter<>();
            buildSingletonPipe(sw).process(s);
            assertEquals(output.get("ecod"),sw.getAndReset());

        } catch (IOException ioEx) {
            assertFalse(true, "Input File: itemEcod.xml not found");
        }
    }

    private static void initializeOutput() {

        output = new HashMap<>();
        output.put("ecod", "'https://data.swissbib.ch/item/ECOD-f1f6f28d-2ceb-3929-baa6-09b30bb1d0e1'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/ECOD-f1f6f28d-2ceb-3929-baa6-09b30bb1d0e1', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/574271538', 'foaf:page': 'https://www.e-codices.ch/en/list/one/kmew/0004', 'bibo:locator': 'Wattwil, Kloster Maria der Engel Wattwil, Ms. 4', 'bibo:owner': 'https://data.swissbib.ch/organisation/ECOD' }");
        output.put("edoc","'https://data.swissbib.ch/item/EDOC-79ab7176-68f0-3fa3-8d17-3d6949a295e1'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/EDOC-79ab7176-68f0-3fa3-8d17-3d6949a295e1', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577885863', 'foaf:page': 'https://edoc.unibas.ch/73459', 'bibo:locator': Article, 'bibo:owner': 'https://data.swissbib.ch/organisation/EDOC' }");
        output.put("alexrepo", "'https://data.swissbib.ch/item/ALEXREPO-d349352b-2d1f-376d-b6b7-6c9cb6445a6e'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/ALEXREPO-d349352b-2d1f-376d-b6b7-6c9cb6445a6e', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/57788428X', 'foaf:page': 'https://www.alexandria.unisg.ch/258823', 'bibo:locator': 'Journal paper', 'bibo:owner': 'https://data.swissbib.ch/organisation/ALEXREPO' }");
        output.put("boris", "'https://data.swissbib.ch/item/BORIS-41fd130f-d742-3cf1-aa41-dc9954bc3a9a'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/BORIS-41fd130f-d742-3cf1-aa41-dc9954bc3a9a', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577812017', 'foaf:page': 'https://boris.unibe.ch/136856', 'bibo:locator': 'info:eu-repo/semantics/article', 'bibo:owner': 'https://data.swissbib.ch/organisation/BORIS' }");
        output.put("zora", "'https://data.swissbib.ch/item/ZORA-ca90a233-1ac9-32aa-891f-0f742788cc1d'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/ZORA-ca90a233-1ac9-32aa-891f-0f742788cc1d', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577995057', 'foaf:page': 'https://www.zora.uzh.ch/id/eprint/173305', 'bibo:locator': 'Journal Article', 'bibo:owner': 'https://data.swissbib.ch/organisation/ZORA' }'https://data.swissbib.ch/item/ZORA-9474f809-238d-3888-8865-98cddae79ed6'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/ZORA-9474f809-238d-3888-8865-98cddae79ed6', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577995057', 'foaf:page': 'https://www.zora.uzh.ch/id/eprint/177578', 'bibo:locator': 'Journal Article', 'bibo:owner': 'https://data.swissbib.ch/organisation/ZORA' }");
        output.put("serval", "'https://data.swissbib.ch/item/SERVAL-f341df89-9637-33ca-a5a9-f4fb38b3f210'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/SERVAL-f341df89-9637-33ca-a5a9-f4fb38b3f210', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/570251621', 'foaf:page': 'http://serval.unil.ch/?id=serval:BIB_F4DCA9C40DDE', 'bibo:locator': incollection, 'bibo:owner': 'https://data.swissbib.ch/organisation/SERVAL' }");
        output.put("sbt", "'https://data.swissbib.ch/item/RERO-ca485892-765f-3ee1-8352-91f56233aab6'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-ca485892-765f-3ee1-8352-91f56233aab6', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/578096110', 'foaf:page': 'http://data.rero.ch/01-R008955607/html?view=FR_V1', 'bibo:locator': 'FSES Z804 TEB 30724A', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE01046' }'https://data.swissbib.ch/item/RERO-c6c65d04-f817-3111-b0c4-d82d682c8706'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-c6c65d04-f817-3111-b0c4-d82d682c8706', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/578096110', 'foaf:page': 'http://data.rero.ch/01-R008955607/html?view=FR_V1', 'bibo:locator': 'FSES Z804 TEB 30724A+A', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE01046' }'https://data.swissbib.ch/item/RERO-48feb625-e7d5-35ba-b812-4b41c3c12029'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-48feb625-e7d5-35ba-b812-4b41c3c12029', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/578096110', 'foaf:page': 'http://data.rero.ch/01-R008955607/html?view=VS_V1', 'bibo:locator': '- -', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE11010' }'https://data.swissbib.ch/item/RERO-6baa4638-8829-3908-b865-da842c57169b'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-6baa4638-8829-3908-b865-da842c57169b', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/578096110', 'foaf:page': 'http://data.rero.ch/01-R008955607/html?view=NJ_V1', 'bibo:locator': 'HEPBE BUREAU RECHERCHE', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE31008' }'https://data.swissbib.ch/item/RERO-0be47f31-3ce3-31d5-ae04-57a58be0572e'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-0be47f31-3ce3-31d5-ae04-57a58be0572e', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/578096110', 'foaf:page': 'http://data.rero.ch/01-R008955607/html?view=NJ_V1', 'bibo:locator': '159.9(02) NELB 14341 Ed.2020', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE31020' }'https://data.swissbib.ch/item/RERO-393c993c-fa48-3b58-853e-ff291e934674'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-393c993c-fa48-3b58-853e-ff291e934674', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/578096110', 'foaf:page': 'http://data.rero.ch/01-R008955607/html?view=GE_V1', 'bibo:locator': '- -', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE61070' }'https://data.swissbib.ch/item/RERO-c9bfc38a-f28c-36c5-b2cb-c2378a900cb3'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-c9bfc38a-f28c-36c5-b2cb-c2378a900cb3', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/578096110', 'foaf:page': 'http://data.rero.ch/01-R008955607/html?view=GE_V1', 'bibo:locator': '- -', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE61070' }'https://data.swissbib.ch/item/RERO-0c9e713e-c0de-3e63-9891-78f4fd656b8d'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-0c9e713e-c0de-3e63-9891-78f4fd656b8d', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/578096110', 'foaf:page': 'http://data.rero.ch/01-R008955607/html?view=GE_V1', 'bibo:locator': '- -', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE61070' }'https://data.swissbib.ch/item/RERO-81dad372-cff3-3306-a765-6a417ce18bf3'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-81dad372-cff3-3306-a765-6a417ce18bf3', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/578096110', 'foaf:page': 'http://data.rero.ch/01-R008955607/html?view=GE_V1', 'bibo:locator': '- -', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE61070' }'https://data.swissbib.ch/item/RERO-a7a3c673-3127-377e-bd77-001695ce80f4'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-a7a3c673-3127-377e-bd77-001695ce80f4', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/578096110', 'foaf:page': 'http://data.rero.ch/01-R008955607/html?view=GE_V1', 'bibo:locator': '- -', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE61070' }'https://data.swissbib.ch/item/RERO-f3f17fa6-7eb7-36ef-b701-304e31a3e6f3'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-f3f17fa6-7eb7-36ef-b701-304e31a3e6f3', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/578096110', 'foaf:page': 'http://data.rero.ch/01-R008955607/html?view=GE_V1', 'bibo:locator': '- -', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE61070' }'https://data.swissbib.ch/item/SBT-0fa5ba42-44e3-31be-af82-f5aed80d8275'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/SBT-0fa5ba42-44e3-31be-af82-f5aed80d8275', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/578096110', 'foaf:page': 'http://aleph.sbt.ti.ch/F?func=item-global&doc_library=SBT01&doc_number=000933586&sub_library=LUBUL', 'bibo:locator': 'BUL A 808.06615 PUB', 'bibo:owner': 'https://data.swissbib.ch/organisation/SBT-LUBUL' }'https://data.swissbib.ch/item/SBT-f8a62372-cbd1-3624-bcc7-142277837665'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/SBT-f8a62372-cbd1-3624-bcc7-142277837665', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/578096110', 'foaf:page': 'http://aleph.sbt.ti.ch/F?func=item-global&doc_library=SBT01&doc_number=000933586&sub_library=LUBUL', 'bibo:locator': 'BUL B 808.06615 PUB', 'bibo:owner': 'https://data.swissbib.ch/organisation/SBT-LUBUL' }");
        output.put("ha", "'https://data.swissbib.ch/item/CHARCH-e2f9143e-3469-310e-a853-fe1febdf9c59'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/CHARCH-e2f9143e-3469-310e-a853-fe1febdf9c59', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/cha1316923', 'foaf:page': 'https://www.helveticarchives.ch/detail.aspx?ID=cha1316923', 'bibo:locator': SLA-GPED, 'bibo:owner': 'https://data.swissbib.ch/organisation/CHARCH-CHARCH02' }");
        output.put("idssgfh", "'https://data.swissbib.ch/item/VAUD-fdd90aea-7723-387b-86be-533f815e27ff'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/VAUD-fdd90aea-7723-387b-86be-533f815e27ff', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577536206', 'foaf:page': 'https://renouvaud.hosted.exlibrisgroup.com/primo-explore/search?vid=41BCULIB_VU2&search_scope=41BCULIB_ALMA_ALL&query=any,contains,991021284296702852&sortby=rank', 'bibo:locator': '- -', 'bibo:owner': 'https://data.swissbib.ch/organisation/VAUD-bcud' }'https://data.swissbib.ch/item/IDSSG-78d8ec78-5142-34b4-b8ab-7d69abe32fbb'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/IDSSG-78d8ec78-5142-34b4-b8ab-7d69abe32fbb', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577536206', 'foaf:page': 'http://aleph.unisg.ch/F?func=item-global&doc_library=HSB01&doc_number=001027327&sub_library=HFHS&local_base=fhs', 'bibo:locator': 'CC 3600 R657(2)', 'bibo:owner': 'https://data.swissbib.ch/organisation/IDSSG-HFHS' }'https://data.swissbib.ch/item/IDSBB-7b016689-fcb6-3b2f-b9dd-1e14f283ebed'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/IDSBB-7b016689-fcb6-3b2f-b9dd-1e14f283ebed', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577536206', 'foaf:page': 'https://baselbern.swissbib.ch/Record/577536206?expandlib=B400#holding-institution-IDSBB-B400', 'bibo:locator': BeM, 'bibo:owner': 'https://data.swissbib.ch/organisation/IDSBB-B400' }'https://data.swissbib.ch/item/IDSBB-3c259fce-e378-33e5-9d74-a63731c8d42a'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/IDSBB-3c259fce-e378-33e5-9d74-a63731c8d42a', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577536206', 'foaf:page': 'https://baselbern.swissbib.ch/Record/577536206?expandlib=A238#holding-institution-IDSBB-A238', 'bibo:locator': 'PHI M I 55 (2)', 'bibo:owner': 'https://data.swissbib.ch/organisation/IDSBB-A238' }");
        output.put("idssgph", "'https://data.swissbib.ch/item/IDSSG-6e069f54-5bc2-3f90-b7f8-3cea2fc74d96'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/IDSSG-6e069f54-5bc2-3f90-b7f8-3cea2fc74d96', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/574210873', 'foaf:page': 'http://aleph.unisg.ch/F?func=item-global&doc_library=HSB01&doc_number=001062664&sub_library=HPHOL&local_base=ph', 'bibo:locator': 'Zugriff: siehe \"Externer Link\"', 'bibo:owner': 'https://data.swissbib.ch/organisation/IDSSG-HPHOL' }");
        output.put("ioc", "'https://data.swissbib.ch/item/CEO-68743a27-490a-3aa6-a9a0-9003e9f34787'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/CEO-68743a27-490a-3aa6-a9a0-9003e9f34787', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/575177284', 'foaf:page': 'https://library.olympic.org/Default/doc/SYRACUSE/209131/', 'bibo:locator': -, 'bibo:owner': 'https://data.swissbib.ch/organisation/CEO-CEO' }");
        output.put("kbthurgau", "'https://data.swissbib.ch/item/KBTG-807357d3-1205-32a3-91ae-fc3ae6590758'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/KBTG-807357d3-1205-32a3-91ae-fc3ae6590758', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/576421308', 'foaf:page': 'http://netbiblio.tg.ch/kbtg/search/notice?noticeID=492203', 'bibo:locator': '914.317 BANC', 'bibo:owner': 'https://data.swissbib.ch/organisation/KBTG-KBS' }'https://data.swissbib.ch/item/LIBIB-23cbc372-7874-3368-bf51-8a1a572d8fc2'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/LIBIB-23cbc372-7874-3368-bf51-8a1a572d8fc2', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/576421308', 'foaf:page': 'http://aleph.lbfl.li/F?func=item-global&doc_library=LLB01&doc_number=000453041&sub_library=FLLB', 'bibo:locator': 913(430.249), 'bibo:owner': 'https://data.swissbib.ch/organisation/LIBIB-FLLB' }");
        output.put("vaud", "'https://data.swissbib.ch/item/VAUD-9a6562f1-afca-32db-b09a-e9132f8ac1c6'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/VAUD-9a6562f1-afca-32db-b09a-e9132f8ac1c6', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577956094', 'foaf:page': 'https://renouvaud.hosted.exlibrisgroup.com/primo-explore/search?vid=41BCULIB_VU2&search_scope=41BCULIB_ALMA_ALL&query=any,contains,991021265931002852&sortby=rank', 'bibo:locator': 'USB 9468', 'bibo:owner': 'https://data.swissbib.ch/organisation/VAUD-bcud' }");
        output.put("hemu", "'https://data.swissbib.ch/item/RERO-0aa926c9-38d1-3d98-b994-3b09659093df'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-0aa926c9-38d1-3d98-b994-3b09659093df', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/567787273', 'foaf:page': 'http://data.rero.ch/01-R008905512/html?view=GE_V1', 'bibo:locator': 'BGE Taf 1146', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE61001' }'https://data.swissbib.ch/item/VAUD-1e3cce32-54d7-3a72-a606-c156a50f081c'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/VAUD-1e3cce32-54d7-3a72-a606-c156a50f081c', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/567787273', 'foaf:page': 'https://renouvaud.hosted.exlibrisgroup.com/primo-explore/search?vid=41BCULIB_VU2&search_scope=41BCULIB_ALMA_ALL&query=any,contains,991021224091902852&sortby=rank', 'bibo:locator': 'RIA 14057 780BAC3Ari', 'bibo:owner': 'https://data.swissbib.ch/organisation/VAUD-bcur' }'https://data.swissbib.ch/item/HEMU-0bf81af2-d606-3428-a05b-6b47c95b51db'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/HEMU-0bf81af2-d606-3428-a05b-6b47c95b51db', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/567787273', 'foaf:page': 'http://opacbiblio.hemu-cl.ch/cgi-bin/koha/opac-detail.pl?biblionumber=65160', 'bibo:locator': '780.92 BACH 50', 'bibo:owner': 'https://data.swissbib.ch/organisation/HEMU-BHCL' }'https://data.swissbib.ch/item/NEBIS-84d4d1e2-7724-36a2-8367-fd7cbc2fbe49'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/NEBIS-84d4d1e2-7724-36a2-8367-fd7cbc2fbe49', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/567787273', 'foaf:page': 'https://recherche.nebis.ch/primo-explore/fulldisplay?docid=ebi01_prod011406024&context=L&vid=NEBIS&search_scope=default_scope&tab=default_tab', 'bibo:locator': '2019 A 27969', 'bibo:owner': 'https://data.swissbib.ch/organisation/NEBIS-Z01' }'https://data.swissbib.ch/item/IDSBB-1ef88ec0-4b3d-38da-ad0d-a5979ae67aa1'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/IDSBB-1ef88ec0-4b3d-38da-ad0d-a5979ae67aa1', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/567787273', 'foaf:page': 'https://baselbern.swissbib.ch/Record/567787273?expandlib=A100#holding-institution-IDSBB-A100', 'bibo:locator': 'UBH kk VI 25553', 'bibo:owner': 'https://data.swissbib.ch/organisation/IDSBB-A100' }'https://data.swissbib.ch/item/IDSBB-e3f7f95b-eb03-3828-8b19-042e08ed53e8'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/IDSBB-e3f7f95b-eb03-3828-8b19-042e08ed53e8', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/567787273', 'foaf:page': 'https://baselbern.swissbib.ch/Record/567787273?expandlib=B400#holding-institution-IDSBB-B400', 'bibo:locator': 'BeM RGA 1202', 'bibo:owner': 'https://data.swissbib.ch/organisation/IDSBB-B400' }");
        output.put("bisch", "'https://data.swissbib.ch/item/BISCH-094e390c-7508-3818-aa7e-071518bc2ef7'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/BISCH-094e390c-7508-3818-aa7e-071518bc2ef7', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/576583650', 'foaf:page': 'https://webopac.bibliotheken-schaffhausen.ch/TouchPoint_touchpoint/perma.do?q=0%3D%22302397%22+IN+%5B2%5D&v=extern&l=de', 'bibo:locator': 'S WA 3622/N8 Lesesaal', 'bibo:owner': 'https://data.swissbib.ch/organisation/BISCH-SHSTM' }");
        output.put("libib", "'https://data.swissbib.ch/item/SGBN-23e72e1d-2f6d-3ebc-8e6d-292c560daa53'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/SGBN-23e72e1d-2f6d-3ebc-8e6d-292c560daa53', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/576816019', 'foaf:page': 'https://sgbn-primo.hosted.exlibrisgroup.com/primo-explore/fulldisplay?docid=41STGKBG_ALEPH001471001&context=L&vid=41STGKBG_VU1&search_scope=41STGKBG-Lokal-PC&tab=default_tab&lang=de_DE', 'bibo:locator': '914.38 POLEN', 'bibo:owner': 'https://data.swissbib.ch/organisation/SGBN-SGFB' }'https://data.swissbib.ch/item/LIBIB-f361e19c-8d6e-39c9-b862-62aa68360794'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/LIBIB-f361e19c-8d6e-39c9-b862-62aa68360794', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/576816019', 'foaf:page': 'http://aleph.lbfl.li/F?func=item-global&doc_library=LLB01&doc_number=000453069&sub_library=FLLB', 'bibo:locator': 913(438), 'bibo:owner': 'https://data.swissbib.ch/organisation/LIBIB-FLLB' }");
        output.put("poster", "'https://data.swissbib.ch/item/CCSA-d2889ea3-fbf3-39b3-8534-3b12a1bfd424'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/CCSA-d2889ea3-fbf3-39b3-8534-3b12a1bfd424', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/557999863', 'foaf:page': 'https://nb-posters.primo.exlibrisgroup.com/discovery/fulldisplay?docid=alma991000906984303978&context=L&vid=41SNL_53_INST:posters&search_scope=MyInstitution&tab=LibraryCatalog', 'bibo:locator': 'Aa 1521', 'bibo:owner': 'https://data.swissbib.ch/organisation/CCSA-gevbge' }");
        output.put("alex", "'https://data.swissbib.ch/item/ALEX-e7c6e030-884d-3c79-a3bd-10de085cf014'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/ALEX-e7c6e030-884d-3c79-a3bd-10de085cf014', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/576596299', 'foaf:page': 'https://www.alexandria.ch/primo-explore/search?query=any,contains,9926029603101791&sortby=rank&vid=ALEX', 'bibo:locator': 'LT LK MIL S50 2020', 'bibo:owner': 'https://data.swissbib.ch/organisation/ALEX-LT' }");
        output.put("snl", "'https://data.swissbib.ch/item/SNL-8d4d9011-14bf-3f48-9fd7-013bb7708ff9'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/SNL-8d4d9011-14bf-3f48-9fd7-013bb7708ff9', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/576348422', 'foaf:page': 'https://nb-helveticat.primo.exlibrisgroup.com/discovery/fulldisplay?docid=alma991018014027003976&context=L&vid=41SNL_51_INST:helveticat&search_scope=MyInstitution&tab=LibraryCatalog', 'bibo:locator': 'N 384421', 'bibo:owner': 'https://data.swissbib.ch/organisation/SNL-NB001' }'https://data.swissbib.ch/item/NEBIS-652267c8-ada8-3715-b071-0620435cda05'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/NEBIS-652267c8-ada8-3715-b071-0620435cda05', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/576348422', 'foaf:page': 'https://recherche.nebis.ch/primo-explore/fulldisplay?docid=ebi01_prod011473173&context=L&vid=NEBIS&search_scope=default_scope&tab=default_tab', 'bibo:locator': 'T 93264', 'bibo:owner': 'https://data.swissbib.ch/organisation/NEBIS-E01' }'https://data.swissbib.ch/item/NEBIS-3ed87c6c-6258-3af9-ba99-140f76081840'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/NEBIS-3ed87c6c-6258-3af9-ba99-140f76081840', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/576348422', 'foaf:page': 'https://recherche.nebis.ch/primo-explore/fulldisplay?docid=ebi01_prod011473173&context=L&vid=NEBIS&search_scope=default_scope&tab=default_tab', 'bibo:locator': '282- 52', 'bibo:owner': 'https://data.swissbib.ch/organisation/NEBIS-E65' }");
        output.put("bgr", "'https://data.swissbib.ch/item/BISCH-d4f1a391-c9ff-3e16-aa5a-b5d0b32ff48b'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/BISCH-d4f1a391-c9ff-3e16-aa5a-b5d0b32ff48b', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/575762020', 'foaf:page': 'https://webopac.bibliotheken-schaffhausen.ch/TouchPoint_touchpoint/perma.do?q=0%3D%22301647%22+IN+%5B2%5D&v=extern&l=de', 'bibo:locator': 'F ES 55 Klima', 'bibo:owner': 'https://data.swissbib.ch/organisation/BISCH-SHAGN' }'https://data.swissbib.ch/item/NEBIS-595c6f77-137b-3d1d-a530-1fb59fcb9443'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/NEBIS-595c6f77-137b-3d1d-a530-1fb59fcb9443', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/575762020', 'foaf:page': 'https://recherche.nebis.ch/primo-explore/fulldisplay?docid=ebi01_prod011072468&context=L&vid=NEBIS&search_scope=default_scope&tab=default_tab', 'bibo:locator': 'TN 551.6 Henn Sta', 'bibo:owner': 'https://data.swissbib.ch/organisation/NEBIS-E57' }'https://data.swissbib.ch/item/NEBIS-3a9acd6e-a4fa-3d26-bed5-95065896639c'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/NEBIS-3a9acd6e-a4fa-3d26-bed5-95065896639c', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/575762020', 'foaf:page': 'https://recherche.nebis.ch/primo-explore/fulldisplay?docid=ebi01_prod011072468&context=L&vid=NEBIS&search_scope=default_scope&tab=default_tab', 'bibo:locator': 1030//512, 'bibo:owner': 'https://data.swissbib.ch/organisation/NEBIS-E99' }'https://data.swissbib.ch/item/BGR-b9ffd4ce-3f2d-3d76-9a64-6807181d5256'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/BGR-b9ffd4ce-3f2d-3d76-9a64-6807181d5256', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/575762020', 'foaf:page': 'http://aleph.gr.ch/F?func=item-global&doc_library=BGR01&doc_number=000683945&sub_library=KBG', 'bibo:locator': 'KBG 43.47 m Henn Sta', 'bibo:owner': 'https://data.swissbib.ch/organisation/BGR-KBG' }'https://data.swissbib.ch/item/ABN-fcd67b5a-8453-37fc-8a76-ecd01702407b'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/ABN-fcd67b5a-8453-37fc-8a76-ecd01702407b', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/575762020', 'foaf:page': 'https://abn-primo.hosted.exlibrisgroup.com/primo-explore/fulldisplay?docid=41ABN_ALEPH_DS000856430&context=L&vid=41ABN_VU1&search_scope=ABN51&tab=default_tab', 'bibo:locator': 551, 'bibo:owner': 'https://data.swissbib.ch/organisation/ABN-AKB' }'https://data.swissbib.ch/item/IDSBB-9f0fdaf2-561b-3736-8e06-622cf2b9fdc4'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/IDSBB-9f0fdaf2-561b-3736-8e06-622cf2b9fdc4', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/575762020', 'foaf:page': 'https://baselbern.swissbib.ch/Record/575762020?expandlib=A198#holding-institution-IDSBB-A198', 'bibo:locator': 'GEO 2.35/242', 'bibo:owner': 'https://data.swissbib.ch/organisation/IDSBB-A198' }'https://data.swissbib.ch/item/IDSBB-00e939f0-b603-3c39-8f9f-0fb84ec858d8'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/IDSBB-00e939f0-b603-3c39-8f9f-0fb84ec858d8', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/575762020', 'foaf:page': 'https://baselbern.swissbib.ch/Record/575762020?expandlib=B467#holding-institution-IDSBB-B467', 'bibo:locator': 'GIUB MK 907', 'bibo:owner': 'https://data.swissbib.ch/organisation/IDSBB-B467' }'https://data.swissbib.ch/item/NEBIS-86f0b835-f068-3fae-92ab-bab721ce2528'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/NEBIS-86f0b835-f068-3fae-92ab-bab721ce2528', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/575762020', 'foaf:page': 'https://recherche.nebis.ch/primo-explore/fulldisplay?docid=ebi01_prod011072468&context=L&vid=NEBIS&search_scope=default_scope&tab=default_tab', 'bibo:locator': B/2019/0033, 'bibo:owner': 'https://data.swissbib.ch/organisation/NEBIS-UFBI' }'https://data.swissbib.ch/item/IDSBB-bd488578-08ab-3139-b877-7a74b3d578c1'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/IDSBB-bd488578-08ab-3139-b877-7a74b3d578c1', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/575762020', 'foaf:page': 'https://baselbern.swissbib.ch/Record/575762020?expandlib=A198#holding-institution-IDSBB-A198', 'bibo:locator': AMK, 'bibo:owner': 'https://data.swissbib.ch/organisation/IDSBB-A198' }'https://data.swissbib.ch/item/IDSBB-10b069d1-957d-3b08-ac8c-f90f09b52dbb'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/IDSBB-10b069d1-957d-3b08-ac8c-f90f09b52dbb', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/575762020', 'foaf:page': 'https://baselbern.swissbib.ch/Record/575762020?expandlib=B400#holding-institution-IDSBB-B400', 'bibo:locator': 'BeM RGA 2403', 'bibo:owner': 'https://data.swissbib.ch/organisation/IDSBB-B400' }");
        output.put("ilu", "'https://data.swissbib.ch/item/RERO-a45f02f1-180e-3b6d-9971-0136fff063c4'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-a45f02f1-180e-3b6d-9971-0136fff063c4', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/567871053', 'foaf:page': 'http://data.rero.ch/01-R008922456/html?view=FR_V1', 'bibo:locator': 'STHD B-1546', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE01030' }'https://data.swissbib.ch/item/RERO-c2428aae-c8f5-393c-a419-91bd24a37c3b'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-c2428aae-c8f5-393c-a419-91bd24a37c3b', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/567871053', 'foaf:page': 'http://data.rero.ch/01-R008922456/html?view=GE_V1', 'bibo:locator': '22/28 FT 36390', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE61064' }'https://data.swissbib.ch/item/IDSLU-accea7d5-95e0-392d-ac53-dd0ec86af42b'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/IDSLU-accea7d5-95e0-392d-ac53-dd0ec86af42b', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/567871053', 'foaf:page': 'https://iluplus.hosted.exlibrisgroup.com/41ZBL:institution_scope:41ZBL_Aleph001364005', 'bibo:locator': 'BE 3700 2020', 'bibo:owner': 'https://data.swissbib.ch/organisation/IDSLU-LUUHL' }'https://data.swissbib.ch/item/NEBIS-cac62a87-d0ec-39b5-9c24-e06cfa40165e'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/NEBIS-cac62a87-d0ec-39b5-9c24-e06cfa40165e', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/567871053', 'foaf:page': 'https://recherche.nebis.ch/primo-explore/fulldisplay?docid=ebi01_prod011365613&context=L&vid=NEBIS&search_scope=default_scope&tab=default_tab', 'bibo:locator': '2019 A 22304', 'bibo:owner': 'https://data.swissbib.ch/organisation/NEBIS-Z01' }");
        output.put("abn", "'https://data.swissbib.ch/item/ABN-e5a47083-72f5-3331-b3d2-114734132a62'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/ABN-e5a47083-72f5-3331-b3d2-114734132a62', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/567896307', 'foaf:page': 'https://abn-primo.hosted.exlibrisgroup.com/primo-explore/fulldisplay?docid=41ABN_ALEPH_DS000851102&context=L&vid=41ABN_VU1&search_scope=ABN51&tab=default_tab', 'bibo:locator': 'KSBA ODA', 'bibo:owner': 'https://data.swissbib.ch/organisation/ABN-KSBA' }'https://data.swissbib.ch/item/ABN-7fc55f97-a42b-3b53-b827-ab184b4584bd'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/ABN-7fc55f97-a42b-3b53-b827-ab184b4584bd', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/567896307', 'foaf:page': 'https://abn-primo.hosted.exlibrisgroup.com/primo-explore/fulldisplay?docid=41ABN_ALEPH_DS000851102&context=L&vid=41ABN_VU1&search_scope=ABN51&tab=default_tab', 'bibo:locator': 'KSBA ODA', 'bibo:owner': 'https://data.swissbib.ch/organisation/ABN-KSBA' }'https://data.swissbib.ch/item/ABN-cb5e389b-f1b2-31cc-8158-7a9fb46871d8'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/ABN-cb5e389b-f1b2-31cc-8158-7a9fb46871d8', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/567896307', 'foaf:page': 'https://abn-primo.hosted.exlibrisgroup.com/primo-explore/fulldisplay?docid=41ABN_ALEPH_DS000851102&context=L&vid=41ABN_VU1&search_scope=ABN51&tab=default_tab', 'bibo:locator': 'KSBA ODA', 'bibo:owner': 'https://data.swissbib.ch/organisation/ABN-KSBA' }");
        output.put("sgbn", "'https://data.swissbib.ch/item/SGBN-81fb86e7-4d79-3e76-a558-5902646b783b'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/SGBN-81fb86e7-4d79-3e76-a558-5902646b783b', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/046438610', 'foaf:page': 'https://sgbn-primo.hosted.exlibrisgroup.com/primo-explore/fulldisplay?docid=41STGKBG_ALEPH000070668&context=L&vid=41STGKBG_VU1&search_scope=41STGKBG-Lokal-PC&tab=default_tab&lang=de_DE', 'bibo:locator': 'AFU SA 310.105.9', 'bibo:owner': 'https://data.swissbib.ch/organisation/SGBN-SGAFU' }'https://data.swissbib.ch/item/SGBN-c80db45a-af37-374e-a29f-20be4b7d2cf1'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/SGBN-c80db45a-af37-374e-a29f-20be4b7d2cf1', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/046438610', 'foaf:page': 'https://sgbn-primo.hosted.exlibrisgroup.com/primo-explore/fulldisplay?docid=41STGKBG_ALEPH000070668&context=L&vid=41STGKBG_VU1&search_scope=41STGKBG-Lokal-PC&tab=default_tab&lang=de_DE', 'bibo:locator': 'KBV 83 0692 -9', 'bibo:owner': 'https://data.swissbib.ch/organisation/SGBN-SGKBV' }'https://data.swissbib.ch/item/SGBN-b8ea5bf2-96d4-39bd-a439-b0ea20ef9011'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/SGBN-b8ea5bf2-96d4-39bd-a439-b0ea20ef9011', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/046438610', 'foaf:page': 'https://sgbn-primo.hosted.exlibrisgroup.com/primo-explore/fulldisplay?docid=41STGKBG_ALEPH000070668&context=L&vid=41STGKBG_VU1&search_scope=41STGKBG-Lokal-PC&tab=default_tab&lang=de_DE', 'bibo:locator': 'JRQ 011,9', 'bibo:owner': 'https://data.swissbib.ch/organisation/SGBN-SGKGB' }'https://data.swissbib.ch/item/SGBN-6fc94fdb-2694-35ee-a9a9-4a3b74651f30'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/SGBN-6fc94fdb-2694-35ee-a9a9-4a3b74651f30', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/046438610', 'foaf:page': 'https://sgbn-primo.hosted.exlibrisgroup.com/primo-explore/fulldisplay?docid=41STGKBG_ALEPH000070668&context=L&vid=41STGKBG_VU1&search_scope=41STGKBG-Lokal-PC&tab=default_tab&lang=de_DE', 'bibo:locator': 'ZMk 009.9', 'bibo:owner': 'https://data.swissbib.ch/organisation/SGBN-SGSTA' }");
        output.put("idsbb", "'https://data.swissbib.ch/item/IDSBB-6f87788f-26e6-3258-8b87-a4a4f1bab09b'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/IDSBB-6f87788f-26e6-3258-8b87-a4a4f1bab09b', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577890735', 'foaf:page': 'https://baselbern.swissbib.ch/Record/577890735?expandlib=B500#holding-institution-IDSBB-B500', 'bibo:locator': VRF_, 'bibo:owner': 'https://data.swissbib.ch/organisation/IDSBB-B500' }'https://data.swissbib.ch/item/IDSBB-dbd892e3-302a-38fe-8118-d180a6e17e78'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/IDSBB-dbd892e3-302a-38fe-8118-d180a6e17e78', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577890735', 'foaf:page': 'https://baselbern.swissbib.ch/Record/577890735?expandlib=B500#holding-institution-IDSBB-B500', 'bibo:locator': VRF_, 'bibo:owner': 'https://data.swissbib.ch/organisation/IDSBB-B500' }'https://data.swissbib.ch/item/IDSBB-df15e956-34d8-332a-8230-2065b0e46e97'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/IDSBB-df15e956-34d8-332a-8230-2065b0e46e97', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577890735', 'foaf:page': 'https://baselbern.swissbib.ch/Record/577890735?expandlib=B500#holding-institution-IDSBB-B500', 'bibo:locator': VRF_, 'bibo:owner': 'https://data.swissbib.ch/organisation/IDSBB-B500' }'https://data.swissbib.ch/item/IDSBB-bf8e001b-b036-3f78-ad22-d1d3d67597ba'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/IDSBB-bf8e001b-b036-3f78-ad22-d1d3d67597ba', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577890735', 'foaf:page': 'https://baselbern.swissbib.ch/Record/577890735?expandlib=B500#holding-institution-IDSBB-B500', 'bibo:locator': VRF_, 'bibo:owner': 'https://data.swissbib.ch/organisation/IDSBB-B500' }'https://data.swissbib.ch/item/IDSBB-9df8a031-9363-37d9-860e-b41a1450773d'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/IDSBB-9df8a031-9363-37d9-860e-b41a1450773d', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577890735', 'foaf:page': 'https://baselbern.swissbib.ch/Record/577890735?expandlib=B500#holding-institution-IDSBB-B500', 'bibo:locator': VRF_, 'bibo:owner': 'https://data.swissbib.ch/organisation/IDSBB-B500' }");
        output.put("rero", "'https://data.swissbib.ch/item/RERO-a95ea594-ef1a-3bc8-af5c-1824acc31633'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-a95ea594-ef1a-3bc8-af5c-1824acc31633', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577218352', 'foaf:page': 'http://data.rero.ch/01-R007910022/html?view=FR_V1', 'bibo:locator': 'SHMOD ABL-113/2', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE01030' }'https://data.swissbib.ch/item/RERO-0955a456-74b0-3a41-b0d9-edfc3c0e0637'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-0955a456-74b0-3a41-b0d9-edfc3c0e0637', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577218352', 'foaf:page': 'http://data.rero.ch/01-R007910022/html?view=FR_V1', 'bibo:locator': 'SHMOD ABL-113/1', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE01030' }'https://data.swissbib.ch/item/RERO-552fa870-caf2-3ba3-a10e-f3c097a3877c'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-552fa870-caf2-3ba3-a10e-f3c097a3877c', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577218352', 'foaf:page': 'http://data.rero.ch/01-R007910022/html?view=FR_V1', 'bibo:locator': 'SHMOD ABL-113/4', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE01030' }'https://data.swissbib.ch/item/RERO-bbb86898-e740-3f32-8a57-8f68441c1ff6'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-bbb86898-e740-3f32-8a57-8f68441c1ff6', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577218352', 'foaf:page': 'http://data.rero.ch/01-R007910022/html?view=FR_V1', 'bibo:locator': 'SHMOD ABL-113/3', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE01030' }'https://data.swissbib.ch/item/RERO-74bd294a-c8ac-3816-bda6-723e67c94363'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/RERO-74bd294a-c8ac-3816-bda6-723e67c94363', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/577218352', 'foaf:page': 'http://data.rero.ch/01-R007910022/html?view=GE_V1', 'bibo:locator': '- -', 'bibo:owner': 'https://data.swissbib.ch/organisation/RERO-RE61001' }");
        output.put("idssg", "'https://data.swissbib.ch/item/IDSSG-e31a5ab9-11f5-3deb-9709-e3d753f9149c'{ @type: 'http://bibframe.org/vocab/HeldItem', @context: 'https://resources.swissbib.ch/item/context.jsonld', @id: 'https://data.swissbib.ch/item/IDSSG-e31a5ab9-11f5-3deb-9709-e3d753f9149c', 'bf:holdingFor': 'https://data.swissbib.ch/bibliographicResource/034574492', 'foaf:page': 'https://aleph.unisg.ch/php/bib_holdings.php?docnr=000350276', 'bibo:locator': 'QV 584 B692', 'bibo:owner': 'https://data.swissbib.ch/organisation/IDSSG-HSG' }");

    }

}
