# Extensions for [`metamorph`](https://github.com/metafacture/metafacture-core/tree/master/metafacture-metamorph)

Provides additional commands for filtering data as well as commands used in Metamorph:

* Filter commands:
    * [ext-filter](#ext-filter): Extends the default filter command in Flux by providing a parameter to implement a "filter not" mechanism
* Commands used in Metamorph:
    * [AuthorHash](#authorhash): Creates a hash value for authors based on different MARC fields.
    * [ItemHash](#itemhash): Creates a hash value for items based on different MARC fields.
    

## AuthorHash

*Creates a hash value for authors based on different MARC fields.*

* Implementation: [org.swissbib.linked.metamorph.functions.AuthorHash](src/main/java/org/swissbib/linked/metamorph/functions/AuthorHash.java)

Resources:
* [Morph definition](https://github.com/linked-swissbib/mfWorkflows/blob/master/src/main/resources/transformation/indexWorkflows/morphModules/authorHash100.xml) for an author name in field 100
* [Morph definition](https://github.com/linked-swissbib/mfWorkflows/blob/master/src/main/resources/transformation/indexWorkflows/morphModules/authorHash700.xml) for an author name in field 700
* [Morph definition](https://github.com/linked-swissbib/mfWorkflows/blob/master/src/main/resources/transformation/indexWorkflows/morphModules/authorHash710.xml) for an organisation name in field 710
* [Morph definition](https://github.com/linked-swissbib/mfWorkflows/blob/master/src/main/resources/transformation/indexWorkflows/morphModules/authorHash711.xml) for an organisation name in field 711


## ext-filter

*Extends the default filter command in Flux by providing a parameter to implement a "filter not" mechanism*

* Implementation: [org.swissbib.linked.metamorph.ExtFilter](src/main/java/org/swissbib/linked/metamorph/ExtFilter.java)
* In: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)
* Out: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)
* Option: `filterNot`: If set to true, returns only records which don't match a certain criteria (Boolean; default: false)

Example: [Show record ids which don't have a title (MARC field 245$a)](https://github.com/sschuepbach/metafacture-examples/tree/master/Swissbib-Extensions/Records-without-titles)


## ItemHash

*Creates a hash value for items based on different MARC fields.*

* Implementation: [org.swissbib.linked.metamorph.functions.ItemHash](src/main/java/org/swissbib/linked/metamorph/functions/ItemHash.java)

Resource: [Morph definition](https://github.com/linked-swissbib/mfWorkflows/blob/master/src/main/resources/transformation/indexWorkflows/itemMorph.xml) which uses the item hash generator
