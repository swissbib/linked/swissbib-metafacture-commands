# metafacture-swissbib

Provides commands used for special purposes within the scope of Swissbib:

* [handle-marcxml-sb](#handle-marcxml-sb): Directly transforms MARC-XML fields to CSV rows like record-id,field,indicator1,indicator2,subfield,value
* [handle-marcxml-sru](#handle-marcxml-sru): Handles MARC-XML files received from the SRU interface of Swissbib


## handle-marcxml-sb

*Directly transforms MARC-XML fields to CSV rows like record-id,field,indicator1,indicator2,subfield,value*

* Implementation: [org.swissbib.linked.swissbib.MarcXmlSbHandler](src/main/java/org/swissbib/linked/swissbib/MarcXmlSbHandler.java)
* In: [org.culturegraph.mf.framework.XmlReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/XmlReceiver.java)
* Out: `java.lang.String`

Example: [1:1 transformation of MARC-XML to CSV](https://github.com/sschuepbach/metafacture-examples/tree/master/Swissbib-Extensions/MARC-CSV)


## handle-marcxml-sru

*Handles MARC-XML files received from the SRU interface of Swissbib*

* Implementation: [org.swissbib.linked.swissbib.MarcXmlSruHandler](src/main/java/org/swissbib/linked/swissbib/MarcXmlSruHandler.java)
* In: [org.culturegraph.mf.framework.XmlReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/XmlReceiver.java)
* Out: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)

Example: [Workflow which queries the Swissbib SRU interface and filters, transforms and dumps the results to a CSV file](https://github.com/sschuepbach/metafacture-examples/tree/master/Swissbib-Extensions/Swissbib-SRU)