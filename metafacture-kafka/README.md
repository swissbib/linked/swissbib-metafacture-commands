# metafacture-kafka

Provides a consumer and a producer for [Apache Kafka](https://kafka.apache.org)

* [write-kafka](#write-kafka): Acts as a producer in a Kafka cluster.
* [read-kafka](#read-kafka): Acts as a Kafka Consumer for Metafacture


## read-kafka

*Acts as a Kafka consumer for Metafacture. A source in Metafacture's perspective.*

* Implementation: [org.swissbib.linked.kafka.MfKafkaConsumer](src/main/java/org/swissbib/linked/kafka/MfKafkaConsumer.java)
* In: `java.lang.String` (the address of the Kafka brokers in the form of `host1:port1,host2:port2` )
* Out: `java.lang.Reader`
* Options:
    * topics: Kafka topics (separated by `,`, required)
    * props: Path to [Kafka consumer properties file](https://kafka.apache.org/documentation/#consumerconfigs) (required, see example in `resources/consumer.properties.example`). You don't need to
    set `bootstrap.servers` as it is already defined as input to command


## write-kafka

*Acts as a producer in a Kafka cluster. A sink in Metafacture's perspective.*

* Implementation: [org.swissbib.linked.kafka.MfKafkaProducer](src/main/java/org/swissbib/linked/kafka/KafkaWriter.java)
* In: `java.lang.Object`
* Out: `java.lang.Void`
* Options:
    * topics: Kafka topics (separated by `,`, required)
    * props: Path to [Kafka producer properties file](https://kafka.apache.org/documentation/#producerconfigs) (required)

Example: [A very small example of using the Kafka consumer](https://github.com/sschuepbach/metafacture-examples/tree/master/Swissbib-Extensions/Kafka-Producer)