package org.swissbib.linked.kafka.helper;


import org.junit.jupiter.api.Test;
import org.swissbib.SbMetadataModel;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.swissbib.linked.kafka.helper.StatusMessageBuilder.Status;


public class TestStatusMessageBuilder {


    @Test
    void testStatusMessageBuilder() {

        StatusMessageBuilder mb = new StatusMessageBuilder("body",Status.create);
        mb.setId("4711");

        assertEquals("4711",mb.getId());
        SbMetadataModel mm =  mb.generateMessageModel();

        assertEquals("body", mm.getData());


    }


}
