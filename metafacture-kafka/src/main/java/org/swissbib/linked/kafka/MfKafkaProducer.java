package org.swissbib.linked.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.metafacture.framework.FluxCommand;
import org.metafacture.framework.ObjectReceiver;
import org.metafacture.framework.annotations.Description;
import org.metafacture.framework.annotations.In;
import org.metafacture.framework.annotations.Out;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * @author Sebastian Schüpbach
 * @version 0.1
 * <p>
 * Created on 03.10.16
 */
@SuppressWarnings("unused")
@Description("Acts as a Kafka producer and sink for a Metafacture workflow.")
@FluxCommand("write-kafka")
@In(Reader.class)
@Out(Void.class)
public class MfKafkaProducer<T> implements ObjectReceiver<T> {

    private List<String> kafkaTopics;
    private Boolean firstRecord = true;
    private Producer<String, String> producer;
    private Properties kafkaProperties = new Properties();


    public void setProps(String path) throws IOException {
        kafkaProperties.load(new FileInputStream(path));
    }

    public void setTopics(String topics) {
        this.kafkaTopics = Arrays.asList(topics.split(","));
    }

    @Override
    public void process(T obj) {
        if (firstRecord) {
            startProducer();
            firstRecord = false;
        }
        for (String topic : kafkaTopics) {
            producer.send(new ProducerRecord<>(topic, obj.toString()));
        }
    }

    @Override
    public void resetStream() {
        producer.close();
        startProducer();
    }

    @Override
    public void closeStream() {
        producer.close();
    }

    private void startProducer() {
        producer = new KafkaProducer<>(kafkaProperties);
    }
}
