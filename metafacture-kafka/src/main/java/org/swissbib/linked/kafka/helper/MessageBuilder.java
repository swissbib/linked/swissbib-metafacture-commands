package org.swissbib.linked.kafka.helper;

import org.swissbib.SbMetadataModel;

public abstract class MessageBuilder {

    protected String body;

    private String id = "";



    public MessageBuilder( String body) {
        this.body = body;
    }

    public abstract SbMetadataModel generateMessageModel();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean hasId() {
        return ! this.id.equals("");
    }


}
