package org.swissbib.linked.kafka;

import org.apache.kafka.common.header.Header;

public class KafkaRecordHeader implements Header {

    private String key;
    private String value;

    public KafkaRecordHeader(String key, String value) {
        this.key = key;
        this.value = value;
    }


    @Override
    public String key() {
        return key;
    }

    @Override
    public byte[] value() {
        return value.getBytes();
    }
}
