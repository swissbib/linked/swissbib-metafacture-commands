package org.swissbib.linked.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.serialization.StringSerializer;
import org.metafacture.framework.MetafactureException;
import org.metafacture.io.ConfigurableObjectWriter;
import org.metafacture.io.FileCompression;
import org.swissbib.SbMetadataModel;
import org.swissbib.SbMetadataSerializer;
import org.swissbib.linked.kafka.helper.StatusMessageBuilder;


import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

/**
 * @author Sebastian Schüpbach
 * @author Günter Hipler
 * @version 0.2
 *          <p>
 *          Created on 24.2.2019
 * @implNote GH: actually I'm not sure if I want/need to use a KafkaWriter Command derived
 * from ConfigurableObjectWriter
 * the second alternative {@link MfKafkaProducer} implements the interface with String.class
 * as input type. I think this is a little bit restrictive
 * For sure I will use the configuration mechanism of {@link MfKafkaProducer} next
 * but first I want to collect a little more experience in how to use the types and how to configure
 * them e.g. in 3rd party libraries
 */
public class KafkaWriter<T> implements ConfigurableObjectWriter<T> {

    private String enconding;
    private FileCompression compression;
    private String header;
    private String separator;
    private String host;
    private Integer port;
    private String kafkaTopic = null;
    private PrintWriter out;
    private Boolean firstRecord = true;
    private final Properties props = new Properties();
    private Producer<String, SbMetadataModel> producer;

    private String bootstrapServers = "localhost:9092";

    private String kafkaValueSerializer = SbMetadataSerializer.class.getName();
    private String kafkaKeySerializer = StringSerializer.class.getName();


    /**
     * Returns the encoding used by the underlying writer.
     *
     * @return current encoding
     */
    @Override
    public String getEncoding() {
        return enconding;
    }

    /**
     * Sets the encoding used by the underlying writer.
     *
     * @param encoding name of the encoding
     */
    @Override
    public void setEncoding(String encoding) {
        this.enconding = encoding;
    }

    /**
     * Returns the compression mode.
     *
     * @return current compression mode
     */
    @Override
    public FileCompression getCompression() {
        return compression;
    }

    /**
     * Sets the compression mode.
     *
     * @param compression Compression mode as String compression
     */
    @Override
    public void setCompression(String compression) {
        this.compression = FileCompression.valueOf(compression);
    }

    /**
     * Sets the compression mode.
     *
     * @param compression Compression mode as FileCompression instance
     */
    @Override
    public void setCompression(FileCompression compression) {
        this.compression = compression;
    }

    /**
     * Returns the header which is output before the first object.
     *
     * @return header string
     */
    @Override
    public String getHeader() {
        return header;
    }

    /**
     * Sets the header which is output before the first object.
     *
     * @param header new header string
     */
    @Override
    public void setHeader(String header) {
        this.header = header;
    }

    /**
     * Returns the footer which is output after the last object.
     *
     * @return footer string
     */
    @Override
    public String getFooter() {
        return null;
    }

    /**
     * Sets the footer which is output after the last object.
     *
     * @param footer new footer string
     */
    @Override
    public void setFooter(String footer) {
        String footer1 = footer;
    }

    /**
     * Returns the separator which is output between objects.
     *
     * @return separator string
     */
    @Override
    public String getSeparator() {
        return separator;
    }

    /**
     * Sets the separator which is output between objects.
     *
     * @param separator new separator string
     */
    @Override
    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(String port) {
        this.port = Integer.parseInt(port);
    }

    public void setKafkaTopic(String kafkaTopic) {
        this.kafkaTopic = kafkaTopic;
    }

    public void setBootstrapServers(String bootstrapServers) {
        this.bootstrapServers = bootstrapServers;
    }

    /**
     * This method is called by upstream modules to trigger the
     * processing of {@code obj}.
     *
     * @param obj the object to be processed
     */
    @Override
    public void process(T obj) {
        if (firstRecord) {
            startProducer();
            firstRecord = false;
        }

        if (obj instanceof StatusMessageBuilder) {

            StatusMessageBuilder mb = (StatusMessageBuilder) obj;

            if (mb.hasId()) {
                producer.send(new ProducerRecord<>(
                        this.kafkaTopic,
                        mb.getId(),
                        mb.generateMessageModel()));
            } else {
                producer.send(new ProducerRecord<>(
                        this.kafkaTopic,
                        mb.generateMessageModel()));
            }
        } else if (obj instanceof String){
            SbMetadataModel mm = new SbMetadataModel();
            mm.setData((String)obj);
            producer.send(new ProducerRecord<>(
                    this.kafkaTopic,
                    mm));
        } else {
            throw new MetafactureException("only MessageBuilder " +
                    "or String types are allowed in KafkaWriter");
        }


    }

    /**
     * Resets the module to its initial state. All unsaved data is discarded. This
     * method may throw {@link UnsupportedOperationException} if the model cannot
     * be reset. This method may be called any time during processing.
     */
    @Override
    public void resetStream() {
        producer.close();
        startProducer();
    }

    /**
     * Notifies the module that processing is completed. Resources such as files or
     * search indexes should be closed. The module cannot be used anymore after
     * closeStream() has been called. The module may be reset, however, so
     * it can be used again. This is not guaranteed to work though.
     * This method may be called any time during processing.
     */
    @Override
    public void closeStream() {
        producer.close();
    }

    private void startProducer() {
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,this.bootstrapServers);
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.RETRIES_CONFIG, 0);
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
        props.put(ProducerConfig.LINGER_MS_CONFIG, 1);
        props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, this.kafkaKeySerializer);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, this.kafkaValueSerializer);

        producer = new KafkaProducer<>(props);
    }

    private ArrayList<Header> getKafkaHeaders(String headerAsString) {

        ArrayList<Header> headers = new ArrayList<>();
        //in this simple version we expect multiple headers in the form
        //key=value###key=value
        Arrays.stream( headerAsString.split("###")).forEach(
                headerString -> {
                    String[] key_value = headerString.split("=");
                    if (key_value.length == 2) {
                        headers.add(new KafkaRecordHeader(key_value[0],key_value[1]));
                    }

                }
        );

        return headers;

    }
}
