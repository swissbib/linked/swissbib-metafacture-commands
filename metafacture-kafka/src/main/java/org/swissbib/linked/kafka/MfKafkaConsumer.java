package org.swissbib.linked.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.metafacture.framework.FluxCommand;
import org.metafacture.framework.ObjectReceiver;
import org.metafacture.framework.annotations.Description;
import org.metafacture.framework.annotations.In;
import org.metafacture.framework.annotations.Out;
import org.metafacture.framework.helpers.DefaultObjectPipe;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * @author Sebastian Schüpbach
 * @version 0.1
 *          <p>
 *          Created on 6/20/17
 */
@SuppressWarnings("unused")
@Description("Acts as a Kafka consumer and input source for a Metafacture workflow.")
@FluxCommand("read-kafka")
@In(String.class)
@Out(Reader.class)
public class MfKafkaConsumer extends DefaultObjectPipe<String, ObjectReceiver<Reader>> {
    private List<String> kafkaTopics;
    private Properties kafkaProperties = new Properties();

    public void setTopics(String topics) {
        this.kafkaTopics = Arrays.asList(topics.split(","));
    }

    public void setProps(String path) throws IOException {
        kafkaProperties.load(new FileInputStream(path));
    }

    @SuppressWarnings("InfiniteLoopStatement")
    @Override
    public void process(String server) {
        kafkaProperties.setProperty("bootstrap.servers", server);
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(kafkaProperties);
        consumer.subscribe(kafkaTopics);
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(3000));
            for (ConsumerRecord<String, String> record : records) {
                getReceiver().process(new StringReader(record.value()));
                consumer.commitSync();
            }
        }

    }
}
