package org.swissbib.linked.kafka.helper;

import org.swissbib.SbMetadataModel;
import org.swissbib.types.CbsActions;

public class StatusMessageBuilder extends MessageBuilder {

    private Status status;

    public enum Status {
        create("CREATE"),
        delete("DELETE"),
        replace("REPLACE"),
        notDefined("NOT_DEFINED");

        String action;

        Status(String action) {
            this.action = action;
        }

        public String getValue() {
            return this.action;
        }

        public static Status fromString(String text) {
            if (text != null) {
                for (Status action : Status.values()) {
                    if (text.equalsIgnoreCase(action.getValue())) {
                        return action;
                    }
                }
            }
            return Status.notDefined;
        }

    }

    public StatusMessageBuilder (String body, Status status) {
        super(body);
        this.status = status;

    }


    @Override
    public SbMetadataModel generateMessageModel() {
        SbMetadataModel mm = new SbMetadataModel();

        mm.setCbsAction(mapStatus());
        mm.setData(this.body);
        return mm;

    }

    private CbsActions mapStatus() {
        CbsActions cbsAction = null;
        switch (status) {
            case create: cbsAction = CbsActions.CREATE;
                break;
            case replace: cbsAction = CbsActions.REPLACE;
                break;
            case delete: cbsAction = CbsActions.DELETE;
                break;
            default: cbsAction = CbsActions.REPLACE;
        }
        return cbsAction;
    }
}
