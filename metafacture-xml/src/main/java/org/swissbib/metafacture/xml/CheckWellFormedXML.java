package org.swissbib.metafacture.xml;

import org.metafacture.framework.FluxCommand;
import org.metafacture.framework.MetafactureException;
import org.metafacture.framework.ObjectReceiver;
import org.metafacture.framework.annotations.Description;
import org.metafacture.framework.annotations.In;
import org.metafacture.framework.annotations.Out;
import org.metafacture.framework.helpers.DefaultObjectPipe;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;

@Description("Check if the xml message in the stream is well formed")
@FluxCommand("check-well-formed-xml")
@In(String.class)
@Out(String.class)
public class CheckWellFormedXML extends
        DefaultObjectPipe<String, ObjectReceiver<String>> {


    SAXParserFactory factory = null;

    public CheckWellFormedXML() {
        factory = SAXParserFactory.newInstance();
        factory.setValidating(false);
        factory.setNamespaceAware(true);
    }

    @Override
    public void process(String obj) {

        try {

            SAXParser parser = factory.newSAXParser();
            XMLReader reader = parser.getXMLReader();
            reader.parse(new InputSource(new ByteArrayInputStream(obj.getBytes())));

        } catch (ParserConfigurationException |
                SAXException |
                IOException ex) {

            String message = "not well formed XML message:  " + obj;
            throw new MetafactureException(message,ex);

        }
        getReceiver().process(obj);
    }


}
