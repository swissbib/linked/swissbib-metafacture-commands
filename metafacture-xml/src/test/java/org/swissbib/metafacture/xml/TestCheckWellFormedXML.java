package org.swissbib.metafacture.xml;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.metafacture.flowcontrol.ObjectExceptionCatcher;
import org.metafacture.framework.MetafactureException;
import org.metafacture.io.LineReader;
import org.swissbib.io.ObjectJavaIoWriter;

import java.io.*;

public class TestCheckWellFormedXML {


    @Test
    void testWellFormedStructureWithTypeAttribute() throws Exception {

        String message2Check = readFileResource("org/swissbib/metafacture/xml/correctXmlWithTypeAttribute.xml");
        CheckWellFormedXML check = new CheckWellFormedXML();
        StringWriter sw = new StringWriter();
        ObjectJavaIoWriter ioWriter = new ObjectJavaIoWriter(sw);

        check.setReceiver(ioWriter);
        check.process(message2Check);

        assertEquals(ioWriter.getCurrentStream().trim(),message2Check.trim());
    }

    @Test
    void testWellFormedStructureWithOutTypeAttribute() throws Exception {

        String message2Check = readFileResource("org/swissbib/metafacture/xml/correctXmlWithoutTypeAttribute.xml");

        CheckWellFormedXML check = new CheckWellFormedXML();
        StringWriter sw = new StringWriter();
        ObjectJavaIoWriter ioWriter = new ObjectJavaIoWriter(sw);

        check.setReceiver(ioWriter);
        check.process(message2Check);

        assertEquals(ioWriter.getCurrentStream().trim(),message2Check.trim());
    }

    @Test
    void testExpectMFException1() throws Exception {

        String message2Check = readFileResource("org/swissbib/metafacture/xml/errorJonas1.xml");

        CheckWellFormedXML check = new CheckWellFormedXML();
        StringWriter sw = new StringWriter();
        ObjectJavaIoWriter ioWriter = new ObjectJavaIoWriter(sw);

        check.setReceiver(ioWriter);

        Assertions.assertThrows(MetafactureException.class, () -> {
            check.process(message2Check);
        });

    }


    @Test
    void testExpectMFException2() throws Exception {

        String message2Check = readFileResource("org/swissbib/metafacture/xml/errorJonas2.xml");
        CheckWellFormedXML check = new CheckWellFormedXML();
        StringWriter sw = new StringWriter();
        ObjectJavaIoWriter ioWriter = new ObjectJavaIoWriter(sw);

        check.setReceiver(ioWriter);

        Assertions.assertThrows(MetafactureException.class, () -> {
            check.process(message2Check);
        });

    }

    @Test
    void testCatchExceptionWithMFExceptionCatcherWithoutError() throws Exception{

        final Reader reader = new InputStreamReader(readFileResourceAsStream("org/swissbib/metafacture/xml/aFewLines.xml"));

        LineReader lr = new LineReader();
        ObjectExceptionCatcher<String> oexc = new ObjectExceptionCatcher<>("exception in Test xml well formed");
        lr.setReceiver(oexc);

        CheckWellFormedXML check = new CheckWellFormedXML();
        StringWriter sw = new StringWriter();
        ObjectJavaIoWriter ioWriter = new ObjectJavaIoWriter(sw);
        check.setReceiver(ioWriter);
        oexc.setReceiver(check);

        lr.process(reader);

        assertEquals(ioWriter.getCurrentStream().trim(),withoutErrors().trim());

    }

    @Test
    void testCatchExceptionWithMFExceptionCatcherWithError() throws Exception{

        final Reader reader = new InputStreamReader(readFileResourceAsStream("org/swissbib/metafacture/xml/aFewLinesWithError.xml"));

        LineReader lr = new LineReader();
        ObjectExceptionCatcher<String> oexc = new ObjectExceptionCatcher<>("exception in Test xml well formed");
        lr.setReceiver(oexc);

        CheckWellFormedXML check = new CheckWellFormedXML();
        StringWriter sw = new StringWriter();
        ObjectJavaIoWriter ioWriter = new ObjectJavaIoWriter(sw);
        check.setReceiver(ioWriter);
        oexc.setReceiver(check);

        lr.process(reader);

        assertEquals(ioWriter.getCurrentStream().trim(),withErrors().trim());

    }



    @Test
    void testCatchExceptionWithMFExceptionCatcherWithContentError() throws Exception{

        final Reader reader = new InputStreamReader(readFileResourceAsStream
                ("org/swissbib/metafacture/xml/aFewLinesWithError.xml"));

        LineReader lr = new LineReader();
        ObjectExceptionCatcher<String> oexc = new ObjectExceptionCatcher<>("exception in Test xml well formed");
        lr.setReceiver(oexc);

        CheckWellFormedXML check = new CheckWellFormedXML();
        StringWriter sw = new StringWriter();
        ObjectJavaIoWriter ioWriter = new ObjectJavaIoWriter(sw);
        check.setReceiver(ioWriter);
        oexc.setReceiver(check);

        lr.process(reader);

        System.out.println(ioWriter.getCurrentStream());


    }


    private String readFileResource(String filename) throws Exception{
        ClassLoader classLoader = TestCheckWellFormedXML.class.getClassLoader();
        try (InputStream is =  classLoader.getResourceAsStream(filename)) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length = 0;
            while ((length = is.read(buffer)) != -1) {
                baos.write(buffer, 0, length);
            }

            return baos.toString("UTF-8");

        } catch (IOException ioexc) {
            throw new Exception(ioexc);
        }

    }

    private InputStream readFileResourceAsStream(String filename) throws Exception {

        ClassLoader classLoader = TestCheckWellFormedXML.class.getClassLoader();
        return classLoader.getResourceAsStream(filename);
    }

    private String withoutErrors() {

        return "<record type=\"Bibliographic\" xmlns=\"http://www.loc.gov/MARC21/slim\"><leader>     cam a22      i 4500</leader><controlfield tag=\"001\">094319723</controlfield><controlfield tag=\"003\">Sz</controlfield><controlfield tag=\"005\">20140211063822.0</controlfield><controlfield tag=\"008\">990707s1994    sz a     c    000 0 eng d</controlfield><datafield ind1=\" \" ind2=\" \" tag=\"020\"><subfield code=\"a\">3-907070-56-9 (geb) : CHF 35.-</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"035\"><subfield code=\"a\">(SNL)vtls001218070</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"035\"><subfield code=\"a\">(Sz)001218070</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"040\"><subfield code=\"a\">Sz</subfield><subfield code=\"c\">Sz</subfield></datafield><datafield ind1=\"1\" ind2=\" \" tag=\"041\"><subfield code=\"a\">eng</subfield><subfield code=\"a\">ger</subfield><subfield code=\"h\">eng</subfield></datafield><datafield ind1=\"2\" ind2=\" \" tag=\"110\"><subfield code=\"a\">Sammlung Eduard von der Heydt (Zürich)</subfield><subfield code=\"9\">140323775</subfield><subfield code=\"8\">Sammlung Eduard von der Heydt (Zürich)</subfield><subfield code=\"8\">AT: Heydt, Sammlung</subfield><subfield code=\"8\">AT: Von Der Heydt, Sammlung</subfield><subfield code=\"8\">AT: VonDer Heydt, Sammlung</subfield><subfield code=\"8\">AT: VonDerHeydt, Sammlung</subfield><subfield code=\"8\">AT: Der Heydt, Sammlung</subfield><subfield code=\"8\">AT: DerHeydt, Sammlung</subfield><subfield code=\"8\">AT: Museum Rietberg Zürich. Sammlung Eduard von der Heydt</subfield></datafield><datafield ind1=\"1\" ind2=\"0\" tag=\"245\"><subfield code=\"a\">Indische Skulpturen der Sammlung Eduard von der Heydt</subfield><subfield code=\"b\">Indian sculptures in the von der Heydt Collection</subfield><subfield code=\"c\">beschreibender Katalog von J.E. van Lohuizen-de Leeuw ; [hrsg. vom] Museum Rietberg, Zurich</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"250\"><subfield code=\"a\">2., unveränd. Aufl.</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"260\"><subfield code=\"a\">Zürich</subfield><subfield code=\"b\">Museum Rietberg</subfield><subfield code=\"c\">1994</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"300\"><subfield code=\"a\">250 S</subfield><subfield code=\"b\">Ill</subfield><subfield code=\"c\">23 x 23 cm</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"504\"><subfield code=\"a\">Literaturverz</subfield></datafield><datafield ind1=\"1\" ind2=\"7\" tag=\"600\"><subfield code=\"a\">Heydt, Eduard von der</subfield><subfield code=\"d\">1882-1964</subfield><subfield code=\"0\">(DE-588)118704486</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"648\"><subfield code=\"a\">Geschichte</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"650\"><subfield code=\"a\">Plastik</subfield><subfield code=\"0\">(DE-588)4046277-8</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"650\"><subfield code=\"a\">Sammlung</subfield><subfield code=\"0\">(DE-588)4128844-0</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"651\"><subfield code=\"a\">Indien</subfield><subfield code=\"0\">(DE-588)4026722-2</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"655\"><subfield code=\"a\">Katalog</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"9\" tag=\"690\"><subfield code=\"a\">Indien</subfield><subfield code=\"x\">Plastik</subfield><subfield code=\"y\">Geschichte</subfield><subfield code=\"x\">Sammlung</subfield><subfield code=\"v\">Katalog</subfield></datafield><datafield ind1=\" \" ind2=\"9\" tag=\"690\"><subfield code=\"a\">Heydt, Eduard von der</subfield><subfield code=\"x\">Sammlung</subfield><subfield code=\"v\">Katalog</subfield></datafield><datafield ind1=\"1\" ind2=\" \" tag=\"700\"><subfield code=\"a\">Lohuizen-de Leeuw</subfield><subfield code=\"D\">Joan E. van</subfield><subfield code=\"9\">143809849</subfield><subfield code=\"8\">Lohuizen-De Leeuw, Joan E van</subfield><subfield code=\"8\">AT: Van Lohuizen-De Leeuw, Johanna Engelberta</subfield><subfield code=\"8\">AT: De Leeuw, Joan E. Van Lohuizen</subfield><subfield code=\"8\">AT: De Leeuw, Johannna E. Van Lohuizen</subfield><subfield code=\"8\">AT: Van Lohuizen-De Leeuw, Joan E</subfield></datafield><datafield ind1=\"2\" ind2=\" \" tag=\"710\"><subfield code=\"a\">Museum Rietberg Zürich</subfield><subfield code=\"9\">140232788</subfield><subfield code=\"8\">Museum Rietberg Zürich</subfield><subfield code=\"8\">AT: Museum Rietberg Zürich. Park-Villa Rieter</subfield><subfield code=\"8\">AT: Park-Villa Rieter Zürich</subfield><subfield code=\"8\">AT: Rietbergmuseum Zürich</subfield><subfield code=\"8\">AT: Rietberg, Museum</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"898\"><subfield code=\"a\">BK040000000</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"949\"><subfield code=\"B\">SNL</subfield><subfield code=\"E\">vtls001218070</subfield><subfield code=\"b\">10010</subfield><subfield code=\"j\">N 240097</subfield><subfield code=\"4\">100</subfield><subfield code=\"p\">1901130589</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"986\"><subfield code=\"a\">SWISSBIB</subfield><subfield code=\"b\">00056267X</subfield></datafield></record>\n" +
                "<record type=\"Bibliographic\" xmlns=\"http://www.loc.gov/MARC21/slim\"><leader>     cam a22      i 4500</leader><controlfield tag=\"001\">094319723</controlfield><controlfield tag=\"003\">Sz</controlfield><controlfield tag=\"005\">20140211063822.0</controlfield><controlfield tag=\"008\">990707s1994    sz a     c    000 0 eng d</controlfield><datafield ind1=\" \" ind2=\" \" tag=\"020\"><subfield code=\"a\">3-907070-56-9 (geb) : CHF 35.-</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"035\"><subfield code=\"a\">(SNL)vtls001218070</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"035\"><subfield code=\"a\">(Sz)001218070</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"040\"><subfield code=\"a\">Sz</subfield><subfield code=\"c\">Sz</subfield></datafield><datafield ind1=\"1\" ind2=\" \" tag=\"041\"><subfield code=\"a\">eng</subfield><subfield code=\"a\">ger</subfield><subfield code=\"h\">eng</subfield></datafield><datafield ind1=\"2\" ind2=\" \" tag=\"110\"><subfield code=\"a\">Sammlung Eduard von der Heydt (Zürich)</subfield><subfield code=\"9\">140323775</subfield><subfield code=\"8\">Sammlung Eduard von der Heydt (Zürich)</subfield><subfield code=\"8\">AT: Heydt, Sammlung</subfield><subfield code=\"8\">AT: Von Der Heydt, Sammlung</subfield><subfield code=\"8\">AT: VonDer Heydt, Sammlung</subfield><subfield code=\"8\">AT: VonDerHeydt, Sammlung</subfield><subfield code=\"8\">AT: Der Heydt, Sammlung</subfield><subfield code=\"8\">AT: DerHeydt, Sammlung</subfield><subfield code=\"8\">AT: Museum Rietberg Zürich. Sammlung Eduard von der Heydt</subfield></datafield><datafield ind1=\"1\" ind2=\"0\" tag=\"245\"><subfield code=\"a\">Indische Skulpturen der Sammlung Eduard von der Heydt</subfield><subfield code=\"b\">Indian sculptures in the von der Heydt Collection</subfield><subfield code=\"c\">beschreibender Katalog von J.E. van Lohuizen-de Leeuw ; [hrsg. vom] Museum Rietberg, Zurich</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"250\"><subfield code=\"a\">2., unveränd. Aufl.</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"260\"><subfield code=\"a\">Zürich</subfield><subfield code=\"b\">Museum Rietberg</subfield><subfield code=\"c\">1994</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"300\"><subfield code=\"a\">250 S</subfield><subfield code=\"b\">Ill</subfield><subfield code=\"c\">23 x 23 cm</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"504\"><subfield code=\"a\">Literaturverz</subfield></datafield><datafield ind1=\"1\" ind2=\"7\" tag=\"600\"><subfield code=\"a\">Heydt, Eduard von der</subfield><subfield code=\"d\">1882-1964</subfield><subfield code=\"0\">(DE-588)118704486</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"648\"><subfield code=\"a\">Geschichte</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"650\"><subfield code=\"a\">Plastik</subfield><subfield code=\"0\">(DE-588)4046277-8</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"650\"><subfield code=\"a\">Sammlung</subfield><subfield code=\"0\">(DE-588)4128844-0</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"651\"><subfield code=\"a\">Indien</subfield><subfield code=\"0\">(DE-588)4026722-2</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"655\"><subfield code=\"a\">Katalog</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"9\" tag=\"690\"><subfield code=\"a\">Indien</subfield><subfield code=\"x\">Plastik</subfield><subfield code=\"y\">Geschichte</subfield><subfield code=\"x\">Sammlung</subfield><subfield code=\"v\">Katalog</subfield></datafield><datafield ind1=\" \" ind2=\"9\" tag=\"690\"><subfield code=\"a\">Heydt, Eduard von der</subfield><subfield code=\"x\">Sammlung</subfield><subfield code=\"v\">Katalog</subfield></datafield><datafield ind1=\"1\" ind2=\" \" tag=\"700\"><subfield code=\"a\">Lohuizen-de Leeuw</subfield><subfield code=\"D\">Joan E. van</subfield><subfield code=\"9\">143809849</subfield><subfield code=\"8\">Lohuizen-De Leeuw, Joan E van</subfield><subfield code=\"8\">AT: Van Lohuizen-De Leeuw, Johanna Engelberta</subfield><subfield code=\"8\">AT: De Leeuw, Joan E. Van Lohuizen</subfield><subfield code=\"8\">AT: De Leeuw, Johannna E. Van Lohuizen</subfield><subfield code=\"8\">AT: Van Lohuizen-De Leeuw, Joan E</subfield></datafield><datafield ind1=\"2\" ind2=\" \" tag=\"710\"><subfield code=\"a\">Museum Rietberg Zürich</subfield><subfield code=\"9\">140232788</subfield><subfield code=\"8\">Museum Rietberg Zürich</subfield><subfield code=\"8\">AT: Museum Rietberg Zürich. Park-Villa Rieter</subfield><subfield code=\"8\">AT: Park-Villa Rieter Zürich</subfield><subfield code=\"8\">AT: Rietbergmuseum Zürich</subfield><subfield code=\"8\">AT: Rietberg, Museum</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"898\"><subfield code=\"a\">BK040000000</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"949\"><subfield code=\"B\">SNL</subfield><subfield code=\"E\">vtls001218070</subfield><subfield code=\"b\">10010</subfield><subfield code=\"j\">N 240097</subfield><subfield code=\"4\">100</subfield><subfield code=\"p\">1901130589</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"986\"><subfield code=\"a\">SWISSBIB</subfield><subfield code=\"b\">00056267X</subfield></datafield></record>\n" +
                "<record type=\"Bibliographic\" xmlns=\"http://www.loc.gov/MARC21/slim\"><leader>     cam a22      i 4500</leader><controlfield tag=\"001\">094319723</controlfield><controlfield tag=\"003\">Sz</controlfield><controlfield tag=\"005\">20140211063822.0</controlfield><controlfield tag=\"008\">990707s1994    sz a     c    000 0 eng d</controlfield><datafield ind1=\" \" ind2=\" \" tag=\"020\"><subfield code=\"a\">3-907070-56-9 (geb) : CHF 35.-</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"035\"><subfield code=\"a\">(SNL)vtls001218070</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"035\"><subfield code=\"a\">(Sz)001218070</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"040\"><subfield code=\"a\">Sz</subfield><subfield code=\"c\">Sz</subfield></datafield><datafield ind1=\"1\" ind2=\" \" tag=\"041\"><subfield code=\"a\">eng</subfield><subfield code=\"a\">ger</subfield><subfield code=\"h\">eng</subfield></datafield><datafield ind1=\"2\" ind2=\" \" tag=\"110\"><subfield code=\"a\">Sammlung Eduard von der Heydt (Zürich)</subfield><subfield code=\"9\">140323775</subfield><subfield code=\"8\">Sammlung Eduard von der Heydt (Zürich)</subfield><subfield code=\"8\">AT: Heydt, Sammlung</subfield><subfield code=\"8\">AT: Von Der Heydt, Sammlung</subfield><subfield code=\"8\">AT: VonDer Heydt, Sammlung</subfield><subfield code=\"8\">AT: VonDerHeydt, Sammlung</subfield><subfield code=\"8\">AT: Der Heydt, Sammlung</subfield><subfield code=\"8\">AT: DerHeydt, Sammlung</subfield><subfield code=\"8\">AT: Museum Rietberg Zürich. Sammlung Eduard von der Heydt</subfield></datafield><datafield ind1=\"1\" ind2=\"0\" tag=\"245\"><subfield code=\"a\">Indische Skulpturen der Sammlung Eduard von der Heydt</subfield><subfield code=\"b\">Indian sculptures in the von der Heydt Collection</subfield><subfield code=\"c\">beschreibender Katalog von J.E. van Lohuizen-de Leeuw ; [hrsg. vom] Museum Rietberg, Zurich</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"250\"><subfield code=\"a\">2., unveränd. Aufl.</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"260\"><subfield code=\"a\">Zürich</subfield><subfield code=\"b\">Museum Rietberg</subfield><subfield code=\"c\">1994</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"300\"><subfield code=\"a\">250 S</subfield><subfield code=\"b\">Ill</subfield><subfield code=\"c\">23 x 23 cm</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"504\"><subfield code=\"a\">Literaturverz</subfield></datafield><datafield ind1=\"1\" ind2=\"7\" tag=\"600\"><subfield code=\"a\">Heydt, Eduard von der</subfield><subfield code=\"d\">1882-1964</subfield><subfield code=\"0\">(DE-588)118704486</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"648\"><subfield code=\"a\">Geschichte</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"650\"><subfield code=\"a\">Plastik</subfield><subfield code=\"0\">(DE-588)4046277-8</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"650\"><subfield code=\"a\">Sammlung</subfield><subfield code=\"0\">(DE-588)4128844-0</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"651\"><subfield code=\"a\">Indien</subfield><subfield code=\"0\">(DE-588)4026722-2</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"655\"><subfield code=\"a\">Katalog</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"9\" tag=\"690\"><subfield code=\"a\">Indien</subfield><subfield code=\"x\">Plastik</subfield><subfield code=\"y\">Geschichte</subfield><subfield code=\"x\">Sammlung</subfield><subfield code=\"v\">Katalog</subfield></datafield><datafield ind1=\" \" ind2=\"9\" tag=\"690\"><subfield code=\"a\">Heydt, Eduard von der</subfield><subfield code=\"x\">Sammlung</subfield><subfield code=\"v\">Katalog</subfield></datafield><datafield ind1=\"1\" ind2=\" \" tag=\"700\"><subfield code=\"a\">Lohuizen-de Leeuw</subfield><subfield code=\"D\">Joan E. van</subfield><subfield code=\"9\">143809849</subfield><subfield code=\"8\">Lohuizen-De Leeuw, Joan E van</subfield><subfield code=\"8\">AT: Van Lohuizen-De Leeuw, Johanna Engelberta</subfield><subfield code=\"8\">AT: De Leeuw, Joan E. Van Lohuizen</subfield><subfield code=\"8\">AT: De Leeuw, Johannna E. Van Lohuizen</subfield><subfield code=\"8\">AT: Van Lohuizen-De Leeuw, Joan E</subfield></datafield><datafield ind1=\"2\" ind2=\" \" tag=\"710\"><subfield code=\"a\">Museum Rietberg Zürich</subfield><subfield code=\"9\">140232788</subfield><subfield code=\"8\">Museum Rietberg Zürich</subfield><subfield code=\"8\">AT: Museum Rietberg Zürich. Park-Villa Rieter</subfield><subfield code=\"8\">AT: Park-Villa Rieter Zürich</subfield><subfield code=\"8\">AT: Rietbergmuseum Zürich</subfield><subfield code=\"8\">AT: Rietberg, Museum</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"898\"><subfield code=\"a\">BK040000000</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"949\"><subfield code=\"B\">SNL</subfield><subfield code=\"E\">vtls001218070</subfield><subfield code=\"b\">10010</subfield><subfield code=\"j\">N 240097</subfield><subfield code=\"4\">100</subfield><subfield code=\"p\">1901130589</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"986\"><subfield code=\"a\">SWISSBIB</subfield><subfield code=\"b\">00056267X</subfield></datafield></record>";
    }

    private String withErrors() {

        return "<record type=\"Bibliographic\" xmlns=\"http://www.loc.gov/MARC21/slim\"><leader>     cam a22      i 4500</leader><controlfield tag=\"001\">094319723</controlfield><controlfield tag=\"003\">Sz</controlfield><controlfield tag=\"005\">20140211063822.0</controlfield><controlfield tag=\"008\">990707s1994    sz a     c    000 0 eng d</controlfield><datafield ind1=\" \" ind2=\" \" tag=\"020\"><subfield code=\"a\">3-907070-56-9 (geb) : CHF 35.-</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"035\"><subfield code=\"a\">(SNL)vtls001218070</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"035\"><subfield code=\"a\">(Sz)001218070</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"040\"><subfield code=\"a\">Sz</subfield><subfield code=\"c\">Sz</subfield></datafield><datafield ind1=\"1\" ind2=\" \" tag=\"041\"><subfield code=\"a\">eng</subfield><subfield code=\"a\">ger</subfield><subfield code=\"h\">eng</subfield></datafield><datafield ind1=\"2\" ind2=\" \" tag=\"110\"><subfield code=\"a\">Sammlung Eduard von der Heydt (Zürich)</subfield><subfield code=\"9\">140323775</subfield><subfield code=\"8\">Sammlung Eduard von der Heydt (Zürich)</subfield><subfield code=\"8\">AT: Heydt, Sammlung</subfield><subfield code=\"8\">AT: Von Der Heydt, Sammlung</subfield><subfield code=\"8\">AT: VonDer Heydt, Sammlung</subfield><subfield code=\"8\">AT: VonDerHeydt, Sammlung</subfield><subfield code=\"8\">AT: Der Heydt, Sammlung</subfield><subfield code=\"8\">AT: DerHeydt, Sammlung</subfield><subfield code=\"8\">AT: Museum Rietberg Zürich. Sammlung Eduard von der Heydt</subfield></datafield><datafield ind1=\"1\" ind2=\"0\" tag=\"245\"><subfield code=\"a\">Indische Skulpturen der Sammlung Eduard von der Heydt</subfield><subfield code=\"b\">Indian sculptures in the von der Heydt Collection</subfield><subfield code=\"c\">beschreibender Katalog von J.E. van Lohuizen-de Leeuw ; [hrsg. vom] Museum Rietberg, Zurich</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"250\"><subfield code=\"a\">2., unveränd. Aufl.</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"260\"><subfield code=\"a\">Zürich</subfield><subfield code=\"b\">Museum Rietberg</subfield><subfield code=\"c\">1994</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"300\"><subfield code=\"a\">250 S</subfield><subfield code=\"b\">Ill</subfield><subfield code=\"c\">23 x 23 cm</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"504\"><subfield code=\"a\">Literaturverz</subfield></datafield><datafield ind1=\"1\" ind2=\"7\" tag=\"600\"><subfield code=\"a\">Heydt, Eduard von der</subfield><subfield code=\"d\">1882-1964</subfield><subfield code=\"0\">(DE-588)118704486</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"648\"><subfield code=\"a\">Geschichte</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"650\"><subfield code=\"a\">Plastik</subfield><subfield code=\"0\">(DE-588)4046277-8</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"650\"><subfield code=\"a\">Sammlung</subfield><subfield code=\"0\">(DE-588)4128844-0</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"651\"><subfield code=\"a\">Indien</subfield><subfield code=\"0\">(DE-588)4026722-2</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"655\"><subfield code=\"a\">Katalog</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"9\" tag=\"690\"><subfield code=\"a\">Indien</subfield><subfield code=\"x\">Plastik</subfield><subfield code=\"y\">Geschichte</subfield><subfield code=\"x\">Sammlung</subfield><subfield code=\"v\">Katalog</subfield></datafield><datafield ind1=\" \" ind2=\"9\" tag=\"690\"><subfield code=\"a\">Heydt, Eduard von der</subfield><subfield code=\"x\">Sammlung</subfield><subfield code=\"v\">Katalog</subfield></datafield><datafield ind1=\"1\" ind2=\" \" tag=\"700\"><subfield code=\"a\">Lohuizen-de Leeuw</subfield><subfield code=\"D\">Joan E. van</subfield><subfield code=\"9\">143809849</subfield><subfield code=\"8\">Lohuizen-De Leeuw, Joan E van</subfield><subfield code=\"8\">AT: Van Lohuizen-De Leeuw, Johanna Engelberta</subfield><subfield code=\"8\">AT: De Leeuw, Joan E. Van Lohuizen</subfield><subfield code=\"8\">AT: De Leeuw, Johannna E. Van Lohuizen</subfield><subfield code=\"8\">AT: Van Lohuizen-De Leeuw, Joan E</subfield></datafield><datafield ind1=\"2\" ind2=\" \" tag=\"710\"><subfield code=\"a\">Museum Rietberg Zürich</subfield><subfield code=\"9\">140232788</subfield><subfield code=\"8\">Museum Rietberg Zürich</subfield><subfield code=\"8\">AT: Museum Rietberg Zürich. Park-Villa Rieter</subfield><subfield code=\"8\">AT: Park-Villa Rieter Zürich</subfield><subfield code=\"8\">AT: Rietbergmuseum Zürich</subfield><subfield code=\"8\">AT: Rietberg, Museum</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"898\"><subfield code=\"a\">BK040000000</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"949\"><subfield code=\"B\">SNL</subfield><subfield code=\"E\">vtls001218070</subfield><subfield code=\"b\">10010</subfield><subfield code=\"j\">N 240097</subfield><subfield code=\"4\">100</subfield><subfield code=\"p\">1901130589</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"986\"><subfield code=\"a\">SWISSBIB</subfield><subfield code=\"b\">00056267X</subfield></datafield></record>\n" +
                "<record type=\"Bibliographic\" xmlns=\"http://www.loc.gov/MARC21/slim\"><leader>     cam a22      i 4500</leader><controlfield tag=\"001\">094319723</controlfield><controlfield tag=\"003\">Sz</controlfield><controlfield tag=\"005\">20140211063822.0</controlfield><controlfield tag=\"008\">990707s1994    sz a     c    000 0 eng d</controlfield><datafield ind1=\" \" ind2=\" \" tag=\"020\"><subfield code=\"a\">3-907070-56-9 (geb) : CHF 35.-</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"035\"><subfield code=\"a\">(SNL)vtls001218070</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"035\"><subfield code=\"a\">(Sz)001218070</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"040\"><subfield code=\"a\">Sz</subfield><subfield code=\"c\">Sz</subfield></datafield><datafield ind1=\"1\" ind2=\" \" tag=\"041\"><subfield code=\"a\">eng</subfield><subfield code=\"a\">ger</subfield><subfield code=\"h\">eng</subfield></datafield><datafield ind1=\"2\" ind2=\" \" tag=\"110\"><subfield code=\"a\">Sammlung Eduard von der Heydt (Zürich)</subfield><subfield code=\"9\">140323775</subfield><subfield code=\"8\">Sammlung Eduard von der Heydt (Zürich)</subfield><subfield code=\"8\">AT: Heydt, Sammlung</subfield><subfield code=\"8\">AT: Von Der Heydt, Sammlung</subfield><subfield code=\"8\">AT: VonDer Heydt, Sammlung</subfield><subfield code=\"8\">AT: VonDerHeydt, Sammlung</subfield><subfield code=\"8\">AT: Der Heydt, Sammlung</subfield><subfield code=\"8\">AT: DerHeydt, Sammlung</subfield><subfield code=\"8\">AT: Museum Rietberg Zürich. Sammlung Eduard von der Heydt</subfield></datafield><datafield ind1=\"1\" ind2=\"0\" tag=\"245\"><subfield code=\"a\">Indische Skulpturen der Sammlung Eduard von der Heydt</subfield><subfield code=\"b\">Indian sculptures in the von der Heydt Collection</subfield><subfield code=\"c\">beschreibender Katalog von J.E. van Lohuizen-de Leeuw ; [hrsg. vom] Museum Rietberg, Zurich</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"250\"><subfield code=\"a\">2., unveränd. Aufl.</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"260\"><subfield code=\"a\">Zürich</subfield><subfield code=\"b\">Museum Rietberg</subfield><subfield code=\"c\">1994</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"300\"><subfield code=\"a\">250 S</subfield><subfield code=\"b\">Ill</subfield><subfield code=\"c\">23 x 23 cm</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"504\"><subfield code=\"a\">Literaturverz</subfield></datafield><datafield ind1=\"1\" ind2=\"7\" tag=\"600\"><subfield code=\"a\">Heydt, Eduard von der</subfield><subfield code=\"d\">1882-1964</subfield><subfield code=\"0\">(DE-588)118704486</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"648\"><subfield code=\"a\">Geschichte</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"650\"><subfield code=\"a\">Plastik</subfield><subfield code=\"0\">(DE-588)4046277-8</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"650\"><subfield code=\"a\">Sammlung</subfield><subfield code=\"0\">(DE-588)4128844-0</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"651\"><subfield code=\"a\">Indien</subfield><subfield code=\"0\">(DE-588)4026722-2</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"7\" tag=\"655\"><subfield code=\"a\">Katalog</subfield><subfield code=\"2\">gnd</subfield></datafield><datafield ind1=\" \" ind2=\"9\" tag=\"690\"><subfield code=\"a\">Indien</subfield><subfield code=\"x\">Plastik</subfield><subfield code=\"y\">Geschichte</subfield><subfield code=\"x\">Sammlung</subfield><subfield code=\"v\">Katalog</subfield></datafield><datafield ind1=\" \" ind2=\"9\" tag=\"690\"><subfield code=\"a\">Heydt, Eduard von der</subfield><subfield code=\"x\">Sammlung</subfield><subfield code=\"v\">Katalog</subfield></datafield><datafield ind1=\"1\" ind2=\" \" tag=\"700\"><subfield code=\"a\">Lohuizen-de Leeuw</subfield><subfield code=\"D\">Joan E. van</subfield><subfield code=\"9\">143809849</subfield><subfield code=\"8\">Lohuizen-De Leeuw, Joan E van</subfield><subfield code=\"8\">AT: Van Lohuizen-De Leeuw, Johanna Engelberta</subfield><subfield code=\"8\">AT: De Leeuw, Joan E. Van Lohuizen</subfield><subfield code=\"8\">AT: De Leeuw, Johannna E. Van Lohuizen</subfield><subfield code=\"8\">AT: Van Lohuizen-De Leeuw, Joan E</subfield></datafield><datafield ind1=\"2\" ind2=\" \" tag=\"710\"><subfield code=\"a\">Museum Rietberg Zürich</subfield><subfield code=\"9\">140232788</subfield><subfield code=\"8\">Museum Rietberg Zürich</subfield><subfield code=\"8\">AT: Museum Rietberg Zürich. Park-Villa Rieter</subfield><subfield code=\"8\">AT: Park-Villa Rieter Zürich</subfield><subfield code=\"8\">AT: Rietbergmuseum Zürich</subfield><subfield code=\"8\">AT: Rietberg, Museum</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"898\"><subfield code=\"a\">BK040000000</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"949\"><subfield code=\"B\">SNL</subfield><subfield code=\"E\">vtls001218070</subfield><subfield code=\"b\">10010</subfield><subfield code=\"j\">N 240097</subfield><subfield code=\"4\">100</subfield><subfield code=\"p\">1901130589</subfield></datafield><datafield ind1=\" \" ind2=\" \" tag=\"986\"><subfield code=\"a\">SWISSBIB</subfield><subfield code=\"b\">00056267X</subfield></datafield></record>\n";
    }


}
