package org.swissbib.metafacture.xml.xslt;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.metafacture.io.FileOpener;
import org.metafacture.io.LineReader;
import org.metafacture.io.ObjectStdoutWriter;
import java.io.*;

class TestMFDocEngine {

    @Disabled("because we do not use the necessary xslt template parser (xslt 2.0) in the general repository")
    @Test
    void firstTransformationWithTemplateStep1() throws Exception {

        ClassLoader classLoader = new TestMFDocEngine().getClass().getClassLoader();

        File startfile = new File(classLoader.getResource
                ("org/swissbib/documentprocessing/step1.xsl").getFile());
        File stopfile = new File(classLoader.getResource
                ("org/swissbib/documentprocessing/step2.xsl").getFile());
        //String content = new String(Files.readAllBytes(file.toPath()));
        //System.out.println(content);
        File datafile = new File(classLoader.getResource
                ("org/swissbib/documentprocessing/testfile.xml.gz").getFile());

        FileOpener fo = new FileOpener();
        LineReader lr = new LineReader();
        fo.setReceiver(lr);

        XSLTPipeStart startTransformer = new XSLTPipeStart();
        //startTransformer.setTemplate(startfile.getAbsolutePath());

        startTransformer.setConfigFile("pipeConfigTest.yaml");

        //you have to use saxon TransformerFactory for xslt 2 syntax
        startTransformer.setTransformerFactory("net.sf.saxon.TransformerFactoryImpl");
        lr.setReceiver(startTransformer);

        XSLTPipeStop pipeStop = new XSLTPipeStop();
        pipeStop.setTemplate(stopfile.getAbsolutePath());
        ObjectStdoutWriter<String> ow = new ObjectStdoutWriter<>();
        pipeStop.setReceiver(ow);

        startTransformer.setReceiver(pipeStop);
        fo.process(datafile.getAbsolutePath());

    }

}
