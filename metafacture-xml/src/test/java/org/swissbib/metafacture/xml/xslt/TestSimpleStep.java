package org.swissbib.metafacture.xml.xslt;


import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.metafacture.io.FileOpener;
import org.metafacture.io.ObjectJavaIoWriter;
import java.io.*;
import org.swissbib.io.FileSplitter;


public class TestSimpleStep {

    @Disabled("because we do not use the necessary xslt template parser (xslt 2.0) in the general repository")
    @Test
    void testSimpleTransformation() {

        //actuall this test doesn't work because I have only xslt-2.0 templates
        //and I don't want to include saxon HE dependency in the general repository

        ClassLoader classLoader = new TestSimpleStep().getClass().getClassLoader();

        File simpleXsltTransformation = new File(classLoader.getResource
                ("xmltojson.xsl").getFile());
        File datafile = new File(classLoader.getResource
                ("org/swissbib/metafacture/xml/xslt/marc21test.xml").getFile());

        FileOpener fo = new FileOpener();

        FileSplitter fs = new FileSplitter();
        fs.setDelimiter("(?<=</record>)");

        XSLTSimpleStep simpleStep = new XSLTSimpleStep();

        //simpleStep.setTransformerFactory();
        simpleStep.setTemplate(simpleXsltTransformation.getPath());

        fo.setReceiver(fs);
        fs.setReceiver(simpleStep);

        StringWriter sw = new StringWriter();
        ObjectJavaIoWriter<String> ow = new ObjectJavaIoWriter <String>(sw);
        simpleStep.setReceiver(ow);
        fo.process(datafile.getPath());

        assertEquals(getJsonTransformation().length(), sw.toString().length());
        assertEquals(getJsonTransformation(), sw.toString());


    }

    private String getJsonTransformation() {
        return  "{\"record\":{\"controlfield\":{\"tag\":\"001\",\"$\":\"11562709X\"},\"datafield\":[{\"tag\":\"035\",\"ind1\":\" \",\"ind2\":\" \",\"subfield\":{\"code\":\"a\",\"$\":\"(OCoLC)298550096\"}},{\"tag\":\"035\",\"ind1\":\" \",\"ind2\":\" \",\"subfield\":{\"code\":\"a\",\"$\":\"(NEBIS)005683299\"}},{\"tag\":909,\"ind1\":\"Z\",\"ind2\":2,\"subfield\":[{\"code\":\"a\",\"$\":\"zbzmon200811f\"},{\"code\":\"c\",\"$\":\"tobt\"},{\"code\":\"d\",\"$\":\"zbzswk200811b\"},{\"code\":\"e\",\"$\":\"boxm\"}]}],\"controlfield\":{\"tag\":\"005\",\"$\":20120702195104.0},\"leader\":\"     cam a22     5u 4500\",\"controlfield\":{\"tag\":\"008\",\"$\":\"080528s2008    gw            00    ger|d\"},\"datafield\":[{\"tag\":\"020\",\"ind1\":\" \",\"ind2\":\" \",\"subfield\":[{\"code\":\"a\",\"$\":\"978-3-85415-425-9\"},{\"code\":\"c\",\"$\":\"Ebr. : sfr 24.30 (freier Pr.), EUR 13.90, EUR 13.90 (AT)\"}]},{\"tag\":\"015\",\"ind1\":\" \",\"ind2\":\" \",\"subfield\":{\"code\":\"a\",\"$\":\"GFR-DNB-08,N24,2459\"}}]}}\n";
    }


}
