# swissbib-metafacture-commands
Contains a set of modules which provide extended functionality to 
[Metafacture](https://github.com/metafacture/metafacture-core).

## Usage

The modules can either be used as plugins in a 
[standalone instance](https://github.com/metafacture/metafacture-core/releases) of Metafacture or programmatically as
dependencies in a [Metafacture project](https://github.com/metafacture/metafacture-core/wiki/Framework-User-Guide).

### Plugin artifacts

```bash
git clone https://gitlab.com/swissbib/linked/swissbib-metafacture-commands
cd swissbib-metafacture-commands
# Either build all artifacts
./gradlew shadowJar
# or build one individually
./gradlew :name_of_module:shadowJar 
# The resulting artifacts can be found in `module_path/build/libs/`
```

Copy the artifacts to the `plugins/` folder of a standalone Metafacture runner.

#### Docker image

There is an experimental Docker image available which provides a
standalone Metafacture instance including the linked-swissbib plugins. 

```
docker pull sschuepbach/mfrunner-sb-5
```

For further instructions see [here](https://hub.docker.com/r/sschuepbach/mfrunner-sb-5/)

### As libraries

You can download the libraries [directly from Gitlab](https://gitlab.com/swissbib/linked/swissbib-metafacture-commands/-/packages). The Maven coordinates are as follows:

```
Group ID:    org.swissbib.linked
Artifact ID: metamorph
Version:     2.0.4-alpha1
```

## Run unit tests

There are only a few unit tests available (hopefully there will be more in
the near future...). To run them type

```bash
./gradlew clean check
```

## List of modules
* [`metafacture-commons`](metafacture-commons/): Common classes used in other modules
* [`metafacture-csv`](metafacture-csv/): Extensions for writing CSV files
* [`metafacture-elasticsearch`](metafacture-elasticsearch/)
* [`metafacture-io`](metafacture-io/)
* [`metafacture-json`](metafacture-json/)
* [`metafacture-kafka`](metafacture-kafka/)
* [`metafacture-linkeddata`](metafacture-linkeddata/)
* [`metafacture-mangling`](metafacture-mangling/)
* [`metafacture-neo4j`](metafacture-neo4j/)
* [`metafacture-swissbib`](metafacture-swissbib/)
* [`metamorph`](metamorph/)
