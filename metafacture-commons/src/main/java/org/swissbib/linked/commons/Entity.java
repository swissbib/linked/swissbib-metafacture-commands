package org.swissbib.linked.commons;

import org.metafacture.framework.StreamReceiver;

import java.util.ArrayList;
import java.util.List;


public final class Entity implements Replayable {
    private final String name;
    private List<Replayable> children = new ArrayList<>();
    private final Boolean isRoot;

    public String getName() {
        return name;
    }

    public List<Replayable> getChildren() {
        return children;
    }

    public Entity(String name) {
        this(name, true);
    }

    private Entity(String name, Boolean isRoot) {
        this.name = name;
        this.isRoot = isRoot;
    }

    public Entity addEntity(String name) {
        Entity entity = new Entity(name, false);
        return addEntity(entity);
    }

    public Entity addEntity(Entity entity) {
        this.children.add(entity);
        return entity;
    }

    public void addChildren(List<Replayable> children) {
        this.children.addAll(children);
    }

    public Literal addLiteral(String name, String value) {
        Literal literal = new Literal(name, value);
        this.children.add(literal);
        return literal;
    }

    public Replayable lookupChild(String name) {
        for (Replayable c : children) {
            if (c.getName().equals(name)) {
                return c;
            }
        }
        return null;
    }

    @Override
    public void replay(StreamReceiver receiver) {
        if (isRoot) {
            receiver.startRecord(name);
        } else {
            receiver.startEntity(name);
        }
        for (Replayable child : children) {
            child.replay(receiver);
        }
        if (isRoot) {
            receiver.endRecord();
        } else {
            receiver.endEntity();
        }
    }
}
