package org.swissbib.linked.commons;

import org.metafacture.framework.StreamReceiver;

public final class Literal implements Replayable {
    private final String name;
    private final String value;

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    Literal(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public void replay(StreamReceiver receiver) {
        receiver.literal(name, value);
    }
}
