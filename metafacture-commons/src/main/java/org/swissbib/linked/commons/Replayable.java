package org.swissbib.linked.commons;

import org.metafacture.framework.StreamReceiver;

interface Replayable {
    void replay(StreamReceiver receiver);

    String getName();
}

