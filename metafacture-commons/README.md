# Extensions for [`metafacture-commons`](https://github.com/metafacture/metafacture-core/tree/master/metafacture-commons)

Provides shared classes for custom Metafacture commands. Contains the following library:

* [`CustomWriter`](src/main/java/org/swissbib/linked/commons/CustomWriter.java)