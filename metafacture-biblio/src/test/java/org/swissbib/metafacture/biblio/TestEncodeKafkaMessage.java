package org.swissbib.metafacture.biblio;

import org.junit.jupiter.api.Test;
import org.swissbib.linked.kafka.helper.StatusMessageBuilder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


class TestEncodeKafkaMessage {

    @Test
    void testSetMetadata() {


        EncodeKafkaMessage em = new EncodeKafkaMessage();
        em.setMetadata("key1=value1###key2=value2");
        assertEquals("key1=value1###key2=value2",em.getMetadataHash());
    }

    @Test
    void testNoHeadersSet() {
        EncodeKafkaMessage em = new EncodeKafkaMessage();
        assertNull(em.getMetadataHash());
    }

    @Test
    void testWrongHeaderFormat() {
        EncodeKafkaMessage em = new EncodeKafkaMessage();
        em.setMetadata("bla blub");
        assertNull(em.getMetadataHash());
    }

    @Test
    void testWrongHeaderFormat1() {
        EncodeKafkaMessage em = new EncodeKafkaMessage();
        em.setMetadata("key1xxvalue1###key2=value2");
        assertEquals("key2=value2",em.getMetadataHash());
    }

    @Test
    void testWrongHeaderFormat2() {
        EncodeKafkaMessage em = new EncodeKafkaMessage();
        em.setMetadata("key1xxvalue1###key2yyvalue2");
        assertNull(em.getMetadataHash());
    }

    @Test
    void testCorrectStatusCreate() {
        EncodeKafkaMessage em = new EncodeKafkaMessage();
        em.setMetadata("status=create");
        assertEquals("status=create",em.getMetadataHash());
    }

    @Test
    void testCorrectStatusDelete() {
        EncodeKafkaMessage em = new EncodeKafkaMessage();
        em.setMetadata("status=delete");
        assertEquals("status=delete",em.getMetadataHash());

    }


    @Test
    void testCorrectStatusReplace() {
        EncodeKafkaMessage em = new EncodeKafkaMessage();
        em.setMetadata("status=replace");
        assertEquals("status=replace",em.getMetadataHash());

    }

    @Test
    void testStatus() {
        EncodeKafkaMessage em = new EncodeKafkaMessage();
        em.setStatus("CREATE");

        assertEquals(StatusMessageBuilder.Status.create, StatusMessageBuilder.Status.fromString(  em.getStatus()));

        em.setStatus("create");
        assertEquals(StatusMessageBuilder.Status.create, StatusMessageBuilder.Status.fromString(  em.getStatus()));

        em.setStatus("creAte");
        assertEquals(StatusMessageBuilder.Status.create, StatusMessageBuilder.Status.fromString(  em.getStatus()));


        em.setStatus("bla");
        assertEquals(StatusMessageBuilder.Status.notDefined, StatusMessageBuilder.Status.fromString(  em.getStatus()));



    }


}
