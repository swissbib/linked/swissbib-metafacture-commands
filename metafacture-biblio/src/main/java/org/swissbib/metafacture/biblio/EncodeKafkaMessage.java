package org.swissbib.metafacture.biblio;

import org.metafacture.framework.ObjectReceiver;
import org.metafacture.framework.helpers.DefaultObjectPipe;
import org.swissbib.linked.kafka.helper.MessageBuilder;
import org.swissbib.linked.kafka.helper.StatusMessageBuilder;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static org.swissbib.linked.kafka.helper.StatusMessageBuilder.Status;

public class EncodeKafkaMessage
        extends DefaultObjectPipe<String, ObjectReceiver<MessageBuilder>> {

    private Pattern pId = Pattern.compile(".*?tag=\"001\".*?>(.*?)</controlfield>.*",Pattern.MULTILINE |
            Pattern.CASE_INSENSITIVE);

    private String metadataString = null;

    private Status status = Status.notDefined;



    public void setMetadata(String metadata) {
        //todo: first idea was to provide Metadata for KafkaHeaders
        //this seems to be deprecated for our use cases
        //until we have a clear picture the old code is still part of the implementation

        StringBuilder tempString = new StringBuilder();

        Arrays.stream( metadata.split("###")).forEach(
                headerString -> {
                    String[] key_value = headerString.split("=");
                    if (key_value.length == 2) {
                            tempString.append(key_value[0]).append("=").append(key_value[1]).append("###");
                    }
                }
        );
        if (tempString.toString().length() > 0) {
            final String s = tempString.toString();
            metadataString = s.substring(0,s.length() - 3);
        }

    }

    public String getStatus() {
        return status.getValue();
    }

    public void setStatus(String status) {
        this.status = Status.fromString(status.toUpperCase());
    }

    public String getMetadataHash() {
        return metadataString;
    }

    public Pattern getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = Pattern.compile(pId, Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
    }

    @Override
    public void process(String obj) {

        StatusMessageBuilder mb = new StatusMessageBuilder(
                obj,this.status
        );
        Matcher m = this.pId.matcher(obj);
        if (m.matches()) {
            mb.setId(m.group(1));
        }

        getReceiver().process(mb);

    }

}
