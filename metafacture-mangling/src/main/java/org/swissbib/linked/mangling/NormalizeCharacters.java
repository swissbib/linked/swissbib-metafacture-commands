package org.swissbib.linked.mangling;

import org.metafacture.framework.FluxCommand;
import org.metafacture.framework.ObjectReceiver;
import org.metafacture.framework.annotations.Description;
import org.metafacture.framework.annotations.In;
import org.metafacture.framework.annotations.Out;
import org.metafacture.framework.helpers.DefaultObjectPipe;
import java.text.Normalizer;

@Description("Unicode Normalisation of Strings, default is NFC - as it is used in our environment")
@FluxCommand("normalize-string")
@In(String.class)
@Out(String.class)
public class NormalizeCharacters  extends
        DefaultObjectPipe<String, ObjectReceiver<String>> {


    private Normalizer.Form normalizerForm = Normalizer.Form.NFC;

    public void setNormalizerForm(Normalizer.Form normalizerForm) {
        this.normalizerForm = normalizerForm;
    }

    @Override
    public void process(String obj) {
        getReceiver().process(Normalizer.normalize(obj, this.normalizerForm));
    }
}
