package org.swissbib.linked.mangling;

import org.metafacture.framework.FluxCommand;
import org.metafacture.framework.ObjectReceiver;
import org.metafacture.framework.annotations.Description;
import org.metafacture.framework.annotations.In;
import org.metafacture.framework.annotations.Out;
import org.metafacture.framework.helpers.DefaultObjectPipe;

import java.util.regex.Pattern;


@Description("Replaces parts of String with user defined pattern, often useful")
@FluxCommand("substitute-string-pattern")
@In(String.class)
@Out(String.class)
public class SubstituteStringPattern extends
        DefaultObjectPipe<String, ObjectReceiver<String>> {

    private Pattern replace = Pattern.compile("");

    private String replacement = "";

    public void setReplace(String replace) {
        this.replace = Pattern.compile(replace,Pattern.DOTALL |
                Pattern.MULTILINE | Pattern.DOTALL);
    }

    public void setReplacement(String replacement) {
        this.replacement = replacement;
    }

    @Override
    public void process(String obj) {

        getReceiver().process(replace.matcher(obj).replaceAll(replacement));
    }
}
