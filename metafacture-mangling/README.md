# Extensions for [`metafacture-mangling`](https://github.com/metafacture/metafacture-core/tree/master/metafacture-mangling)

Provides commands for deflecting the Metafacture workflow:

* [split-entities](#split-entities): Splits entities into individual records.


## split-entitites

*Splits entities into individual records.*

* Implementation: [org.swissbib.linked.mangling.EntitySplitter](src/main/java/org/swissbib/linked/mangling/EntitySplitter.java)
* In: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)
* Out: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)
* Option: `entityBoundary`: Node depth for entity splitting

Example: [linked-swissbib "Baseline"](https://github.com/sschuepbach/metafacture-examples/tree/master/Swissbib-Extensions/Linked-Swissbib-Baseline)