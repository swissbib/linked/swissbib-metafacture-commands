package org.swissbib.io;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCBSExportSource {

    @Disabled("no testCBSExportSourceToFiles because we do not have any http connection " +
            "to cbs servers outside the debugging environment")
    @Test
    void testCBSExportSourceToFiles() {


        CBSExportSource exportSource =  new CBSExportSource();
        exportSource.setFileRangePattern("job7r4A[1-1].raw");

        FileSplitter fs = new FileSplitter();
        fs.setDelimiter("(?<=</record>)");

        ObjectSizedFilesWriter<String> objectFileWriter = new ObjectSizedFilesWriter<>();

        objectFileWriter.setFileSizeNumberRecords("20000");
        objectFileWriter.setCompression("gzip");
        objectFileWriter.setPath("/usr/local/swissbib/swissbib-metafacture-commands/staff/testdata/export.swissbibToslsp.${i}.xml.gz");

        objectFileWriter.setHeader("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<collection>\n");
        //add carriage return to footer for the last file
        objectFileWriter.setFooter("\n</collection>");

        fs.setReceiver(objectFileWriter);
        exportSource.setReceiver(fs);

        exportSource.process("http://localhost:40000/JOB7/RUN4/");

    }



}
