package org.swissbib.io;

import org.metafacture.framework.FluxCommand;
import org.metafacture.framework.MetafactureException;
import org.metafacture.framework.ObjectReceiver;
import org.metafacture.framework.annotations.Description;
import org.metafacture.framework.annotations.In;
import org.metafacture.framework.annotations.Out;
import org.metafacture.framework.helpers.DefaultObjectPipe;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Description("Fetches CBS exported data files via http which are provided on a directory of the CBS datahub")
@FluxCommand("cbs-export-source")
@In(String.class)
@Out(Reader.class)
public class CBSExportSource
        extends DefaultObjectPipe<String, ObjectReceiver<Reader>> {

    private Optional<String> fileRangePattern = Optional.empty();


    public void setFileRangePattern(String fileRangePattern) {
        this.fileRangePattern = Optional.of(fileRangePattern);
    }

    @Override
    public void process(String obj) {

        Iterator<String> it = this.urls(obj).iterator();
        it.forEachRemaining(url -> {
            try {
                final URL u = new URL(it.next());
                final URLConnection con = u.openConnection();
                getReceiver().process(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8));
            } catch (IOException ioex) {
                throw new MetafactureException(ioex);
            }

        });
        getReceiver().closeStream();
    }

    private Iterable<String> urls(String baseUrl) {

        //very complicated - Kotlin to the rescue....
        Pattern p = Pattern.compile(".*?\\[(\\d*?-\\d*?)].*?");

        final String bU = baseUrl.endsWith("/") ?
                baseUrl :  baseUrl.concat("/");


        ArrayList<String> urls = new ArrayList<>();

        if (this.fileRangePattern.isPresent() ) {

            Matcher m = p.matcher(this.fileRangePattern.get());
            if (m.matches()) {

                String range =  m.group(1);
                String[] edges = range.split("-");

                Pattern p1 = Pattern.compile("(.*?)\\[.*?](.*?)");
                Matcher m1 = p1.matcher(this.fileRangePattern.get());
                if (m1.matches()) {
                    int e1 = Integer.parseInt(edges[0]);
                    int e2 = Integer.parseInt(edges[1]);
                    String part1 = m1.group(1);
                    String part2 = m1.group(2);
                    for (int i = e1; i <= e2; i++) {
                        urls.add(bU + part1 +  String.format("%03d", i) + part2);
                    }
                }
            }

        } else {
            urls.add(bU);
        }
        return urls;
    }
}
