/*
 * Copyright 2013, 2014 Deutsche Nationalbibliothek
 *
 * Licensed under the Apache License, Version 2.0 the "License";
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.swissbib.io;

import org.metafacture.framework.FluxCommand;
import org.metafacture.framework.MetafactureException;
import org.metafacture.framework.annotations.In;
import org.metafacture.io.AbstractObjectWriter;
import org.metafacture.io.FileCompression;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @param <T>
 *            object type
 *
 * @author Markus Geipel
 * @author Christoph Böhme
 * @author Günter Hipler, swissbib
 *
 */
@In(Object.class)
@FluxCommand("write-sized-files")
public class ObjectSizedFilesWriter<T> extends AbstractObjectWriter<T>  {

    private static final String VAR = "${i}";
    private static final Pattern VAR_PATTERN = Pattern.compile(VAR, Pattern.LITERAL);

    private String path = "/data/content${i}.txt";
    private int count;
    private Writer writer;
    private boolean firstObject;
    private boolean closed;
    private int fileSizeNumberRecords = 0;
    private int numberProcessedRecords = 0;
    private boolean constructorInitialized;

    private String encoding = "UTF-8";
    private FileCompression compression = FileCompression.AUTO;

    public ObjectSizedFilesWriter(final String path) {
        super();
        init(path);
        constructorInitialized = true;
    }

    public ObjectSizedFilesWriter() {
        super();
        firstObject = true;
        constructorInitialized = false;
    }


    public void setPath(String path) {
        this.path = path;
    }


    @Override
    public String getEncoding() {
        return encoding;
    }

    @Override
    public void setEncoding(final String encoding) {
        this.encoding = encoding;
    }

    @Override
    public FileCompression getCompression() {
        return compression;
    }

    @Override
    public void setCompression(final FileCompression compression) {
        this.compression = compression;
    }

    @Override
    public void setCompression(final String compression) {
        setCompression(FileCompression.valueOf(compression.toUpperCase()));
    }

    public int getFileSizeNumberRecords() {
        return fileSizeNumberRecords;
    }

    public void setFileSizeNumberRecords(String fileSizeNumberRecords) {
        this.fileSizeNumberRecords = Integer.valueOf(fileSizeNumberRecords);
    }

    @Override
    public void process(final T obj) {
        assert !closed;

        try {
            if (firstObject) {
                if (!constructorInitialized) init(path);
                writer.write(getHeader());
                firstObject = false;
            } else {
                writer.write(getSeparator());
            }
            if (fileSizeNumberRecords != 0 && numberProcessedRecords > 0) {
                if (numberProcessedRecords % fileSizeNumberRecords == 0) {
                    resetStream();
                    writer.write(getHeader());
                }
            }
            if (fileSizeNumberRecords > 0) {
                ++numberProcessedRecords;
            }
            writer.write(obj.toString());
        } catch (final IOException e) {
            throw new MetafactureException(e);
        }
    }

    @Override
    public void resetStream() {
        makeFileHandling();
        ++count;
        startNewFile();
    }

    @Override
    public void closeStream() {
        makeFileHandling();
    }

    private void makeFileHandling() {
        if (!closed) {
            try {
                if (!firstObject) {
                    writer.write(getFooter());
                }
                writer.flush();
                writer.close();
            } catch (final IOException e) {
                throw new MetafactureException(e);
            } finally {
                closed = true;
            }
        }

    }

    private void startNewFile() {
        final Matcher matcher = VAR_PATTERN.matcher(this.path);
        final String path = matcher.replaceAll(String.valueOf(count));
        try {
            final OutputStream file = new FileOutputStream(path);
            try {
                final OutputStream compressor = compression.createCompressor(file, path);
                try {
                    writer = new OutputStreamWriter(compressor, encoding);
                    closed = false;
                } catch (final IOException e) {
                    compressor.close();
                    throw e;
                }
            } catch (final IOException e) {
                file.close();
                throw e;
            }
        } catch (final IOException e) {
            throw new MetafactureException("Error creating file '" + path + "'.", e);
        }
    }

    private void init(String path) {

        this.path = path;
        startNewFile();

        final Matcher matcher = VAR_PATTERN.matcher(this.path);
        if (!matcher.find()) {
            this.path = this.path + VAR;
        }

    }

}
