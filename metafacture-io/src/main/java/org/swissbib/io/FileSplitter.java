package org.swissbib.io;

import org.metafacture.framework.FluxCommand;
import org.metafacture.framework.ObjectReceiver;
import org.metafacture.framework.annotations.Description;
import org.metafacture.framework.annotations.In;
import org.metafacture.framework.annotations.Out;
import org.metafacture.framework.helpers.DefaultObjectPipe;

import java.io.Reader;
import java.util.Scanner;


@Description("tokenizes (split) input streams with a defined pattern")
@FluxCommand("file-splitter")
@In(Reader.class)
@Out(String.class)
public class FileSplitter
        extends DefaultObjectPipe<Reader, ObjectReceiver<String>> {

    //for cbs exported content we use the pattern
    //"(?<=</record>)"
    //for this case we use non-capturing parenthesis - compare Jeffrey E.F. Friedl
    private String delimiter = "\n";

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    @Override
    public void process(Reader obj) {
        Scanner s = new Scanner(obj);
        s.useDelimiter(this.delimiter);
        while (s.hasNext()) {
            getReceiver().process(s.next());
        }
    }
}
