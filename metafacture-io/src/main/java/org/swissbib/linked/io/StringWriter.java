package org.swissbib.linked.io;

import org.metafacture.io.ConfigurableObjectWriter;
import org.metafacture.io.FileCompression;

@SuppressWarnings({"WeakerAccess", "unused"})
public class StringWriter<T> implements ConfigurableObjectWriter<T> {

    private StringBuilder sb;

    public StringWriter() {
        sb = new StringBuilder();
    }

    @Override
    public String getEncoding() {
        return null;
    }

    @Override
    public void setEncoding(String encoding) {

    }

    @Override
    public FileCompression getCompression() {
        return null;
    }

    @Override
    public void setCompression(FileCompression compression) {

    }

    @Override
    public void setCompression(String compression) {

    }

    @Override
    public String getHeader() {
        return null;
    }

    @Override
    public void setHeader(String header) {

    }

    @Override
    public String getFooter() {
        return null;
    }

    @Override
    public void setFooter(String footer) {

    }

    @Override
    public String getSeparator() {
        return null;
    }

    @Override
    public void setSeparator(String separator) {

    }

    @Override
    public void process(T obj) {
        sb.append(obj);
    }

    @Override
    public void resetStream() {
        sb.setLength(0);
    }

    @Override
    public void closeStream() {

    }

    /**
     * Returns cached string
     *
     * @return String cached string
     */
    public String get() {
        return sb.toString();
    }

    /**
     * Returns cached string and empties cache
     *
     * @return String cached string
     */
    public String getAndReset() {
        String str = get();
        resetStream();
        return str;
    }
}
