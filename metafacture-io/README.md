# Extensions for [`metafacture-io`](https://github.com/metafacture/metafacture-core/tree/master/metafacture-io)

Provides commands for handling I/O:

* [write-socket](#write-socket): Sets up a socket server.
* [open-multi-http](#open-multi-http): Allows to open HTTP resources in a "paging" manner, e.g. to get data by chunks from a database


## open-multi-http

*Allows to open HTTP resources in a "paging" manner, e.g. to get data by chunks from a database. You have to define two variable parts in the URL: `${cs}`, which sets the chunk size, and `${pa}`, which sets the offset.*

* Implementation: [org.swissbib.linked.io.MultiHttpOpener](src/main/java/org/swissbib/linked/io/MultiHttpOpener.java)
* In: `java.lang.String`
* Out: `java.lang.Reader`
* Options:
    * accept: The accept header in the form type/subtype, e.g. text/plain.
    * encoding: The encoding is used to encode the output and is passed as Accept-Charset to the http connection.
    * lowerBound: Initial offset
    * upperBound: Limit
    * chunkSize: Number of documents to be downloaded in a single retrieval

Example: [Workflow which queries the Swissbib SRU interface and filters, transforms and dumps the results to a CSV file](https://github.com/sschuepbach/metafacture-examples/tree/master/Swissbib-Extensions/Swissbib-SRU)


## write-socket

*Sets up a socket server*

* Implementation: [org.swissbib.linked.io.SocketWriter](src/main/java/org/swissbib/linked/io/SocketWriter.java)
* In: `java.lang.Object`
* Out: `java.lang.Void`
* Option: `port`: Port of socket server (host is always 127.0.0.1)

Example: [Stream MARC-XML to socket](https://github.com/sschuepbach/metafacture-examples/tree/master/Swissbib-Extensions/Socket-Sink)
