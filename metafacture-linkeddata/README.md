# Extensions for [`metafacture-linkeddata`](https://github.com/metafacture/metafacture-core/tree/master/metafacture-linkeddata)

Provides commands for handling Linked Data and the Elasticsearch bulk format:

* [decode-ntriples](#decode-ntriples): Parses Ntriples files
* [encode-esbulk](#encode-esbulk): Encodes data as JSON-LD or in a special format suitable for bulk indexing in Elasticsearch
* [encode-ntriples](#encode-ntriples): Encodes data as Ntriples
* [write-esbulk](#write-esbulk): Writes records as JSON files which can comply with the requirements of the Bulk API of Elasticsearch.
* [write-rdf-1line](#write-rdf-1line): Writes RDF-XML files, one line per record.


## decode-ntriples

*Parses Ntriples-encoded records.*

* Implementation: [org.swissbib.linked.mf.decoder.NtriplesDecoder](https://github.com/linked-swissbib/swissbib-metafacture-commands/blob/master/src/main/java/org/swissbib/linked/mf/decoder/NtriplesDecoder.java)
* In: `java.io.Reader`
* Out: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)
* Options: 
    * `unicodeEscapeSeq`: "true", "false"
    * `keepLanguageTags`: "true", "false". If set to true (the default), the tag is separated from the literal by two `#`.
    * `keepTypeAnnotations`: "true", "false". If set to true (the default), the annotation is separated from the literal by two `#`.

Example: [linked-swissbib "EnrichedLine"](https://github.com/sschuepbach/metafacture-examples/tree/master/Swissbib-Extensions/Linked-Swissbib-Enrichedline)


## encode-esbulk

*Encodes records for bulk uploading to Elasticsearch.*

* Implementation: [org.swissbib.linked.mf.pipe.ESBulkEncoder](https://github.com/linked-swissbib/swissbib-metafacture-commands/blob/master/src/main/java/org/swissbib/linked/mf/pipe/ESBulkEncoder.java)
* In: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)
* Out: `java.lang.String`
* Options:
    * avoidMergers: If set to true, fields with same keys are modelled as separate inner objects instead of having their values merged (Boolean; default: false)
    * header: Should header for ES bulk be written (Boolean; default: true)? Warning: Setting this parameter to false will result in an invalid Bulk format!
    * escapeChars: Escapes prohibited characters in JSON strings (Boolean; default: true)
    * index: Index name of records

Example: [linked-swissbib "Baseline"](https://github.com/sschuepbach/metafacture-examples/tree/master/Swissbib-Extensions/Linked-Swissbib-Baseline)


## encode-ntriples

*Encodes data as Ntriples*

* Implementation: [org.swissbib.linked.mf.pipe.NtriplesEncoder](https://github.com/linked-swissbib/swissbib-metafacture-commands/blob/master/src/main/java/org/swissbib/linked/mf/pipe/NtriplesEncoder.java)
* In: [org.culturegraph.mf.framework.StreamReceiver](https://github.com/culturegraph/metafacture-core/blob/master/src/main/java/org/culturegraph/mf/framework/StreamReceiver.java)
* Out: `java.lang.String`

Example: [Libadmin entries as Ntriples](https://github.com/sschuepbach/metafacture-examples/blob/master/Swissbib-Extensions/Libadmin-Ntriples/libadminFlux.flux)


## write-esbulk

*Writes records as JSON files which comply with the requirements of the Bulk API of Elasticsearch.*

* Implementation: [org.swissbib.linked.mf.writer.ESBulkWriter](https://github.com/linked-swissbib/swissbib-metafacture-commands/blob/master/src/main/java/org/swissbib/linked/mf/writer/ESBulkWriter.java)
* In: `java.lang.Object`
* Out: `java.lang.Void`
* Options:
    * compress: Should files be .gz-compressed? (Default is true)
    * filePrefix: Prefix for file names
    * fileSize: Number of records in one file
    * jsonCompliant: Should files be JSON compliant (Boolean; default: false)? Warning: Setting this parameter to true will result in an invalid Bulk format!
    * outDir: Root directory for output
    * subdirSize: Number of files in one subdirectory (Default: 300)

Example: [linked-swissbib "Baseline"](https://github.com/sschuepbach/metafacture-examples/tree/master/Swissbib-Extensions/Linked-Swissbib-Baseline)


## write-rdf-1line

*Writes RDF-XML files, one line per record.*

* Implementation: [org.swissbib.linked.linkeddata.SingleLineWriterRDFXml](src/main/java/org/swissbib/linked/linkeddata/SingleLineWriterRDFXml.java)
* In: `java.lang.Object`
* Out: `java.lang.Void`
* Options:
    * usecontributor: "true", "false"
    * rootTag: XML root tag
    * extension: File extension for output files
    * compress: Should output files be compressed? ("true", "false")
    * baseOutDir: Base directory for output files:
    * outFilePrefix: Prefix for output files
    * fileSize: Number of records in one file
    * subDirSize: Number of records in one subdirectory
    * type: Concept / type name

Example: [Deprecated linked-swissbib "baseline" for bibliographicResource documents (use resourceTransformation.rdfXml.flux)](https://github.com/linked-swissbib/mfWorkflows/tree/unused-transformations/src/main/resources/transformation/resource)
