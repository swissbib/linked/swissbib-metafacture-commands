package org.swissbib.linked.linkeddata;

import org.metafacture.framework.MetafactureException;
import org.metafacture.framework.StreamReceiver;
import org.metafacture.framework.annotations.Description;
import org.metafacture.framework.annotations.In;
import org.metafacture.framework.annotations.Out;
import org.metafacture.framework.helpers.DefaultObjectPipe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.swissbib.linked.commons.Entity;

import java.io.IOException;
import java.io.Reader;
import java.util.*;


/**
 * Decodes data in N-triple format to Formeta
 *
 * @author Sebastian Schüpbach
 * @version 1.1
 */
@Description("Decodes lines of N-Triple files.")
@In(Reader.class)
@Out(StreamReceiver.class)
public final class NtriplesDecoder extends DefaultObjectPipe<Reader, StreamReceiver> {

    private Boolean unicodeEscapeSeq = true;
    private char lineSeparator = 0x000a;
    private Boolean keepLanguageTags = true;
    private Boolean keepTypeAnnotations = true;
    private Boolean eachLineIsRecord = true;

    private static final int BUFFER_SIZE = 1024 * 1024 * 16;
    private final char[] buffer = new char[BUFFER_SIZE];

    private int lineNo = 1;
    private Map<String, Element> bnodeMap = new HashMap<>();
    private List<Element> rootBnodes = new ArrayList<>();
    private Map<String, Entity> nameResourceTable = new HashMap<>();
    private Map<String, Entity> nameBNodeTable = new HashMap<>();
    private Map<Entity, String> entityBNodeTable = new HashMap<>();

    private static final Logger LOG = LoggerFactory.getLogger(NtriplesDecoder.class);

    /**
     * Converts Unicode escape sequences in strings to regular UTF-8 encoded characters
     *
     * @param literal String to be checked for Unicode escape sequences
     * @return String with converted Unicode escape sequences
     */
    private static String toutf8(String literal) {
        StringBuilder utf8String = new StringBuilder();
        StringBuilder tempString = new StringBuilder();
        char lastArtifact = ' ';
        char secondLastArtifact = ' ';
        int i = 0;
        int stringLength = literal.length();
        boolean unicodeChar = false;
        while (i < stringLength) {
            char c = literal.charAt(i);
            if (unicodeChar) {
                tempString.append(c);
                if (tempString.length() >= 4) {
                    // Converts hexadecimal code represented as String to integer and casts it afterwards to char
                    c = (char) (int) Integer.valueOf(tempString.toString(), 16);
                    tempString.setLength(0);
                    unicodeChar = false;
                }
            } else {
                // Unicode escape sequences are initialized with \\u. So we have to check for such a tuple,
                // but at the same time have to make sure that this tuple is not escaped either (i.e. a literal \\u).
                if (secondLastArtifact != '\\' && lastArtifact == '\\' && c == 'u') {
                    unicodeChar = true;
                } else {
                    utf8String.append(lastArtifact);
                }
            }
            secondLastArtifact = lastArtifact;
            lastArtifact = c;
            i++;
            if (i == stringLength) utf8String.append(c);
        }
        return utf8String.toString().substring(1);
    }

    @SuppressWarnings("unused")
    public void setUnicodeEscapeSeq(String unicodeEscapeSeq) {
        this.unicodeEscapeSeq = Boolean.valueOf(unicodeEscapeSeq);
    }

    @SuppressWarnings("unused")
    public void setKeepLanguageTags(String keepLanguageTags) {
        this.keepLanguageTags = Boolean.valueOf(keepLanguageTags);
    }

    @SuppressWarnings("unused")
    public void setKeepTypeAnnotations(String keepTypeAnnotations) {
        this.keepTypeAnnotations = Boolean.valueOf(keepTypeAnnotations);
    }

    @SuppressWarnings("unused")
    public void setEachLineIsRecord(String eachLineIsRecord) {
        this.eachLineIsRecord = Boolean.valueOf(eachLineIsRecord);
    }

    @Override
    public void process(Reader reader) {
        try {
            boolean nothingRead = true;
            int size;
            int offset = 0;
            lineNo = 0;
            nameResourceTable.clear();
            nameBNodeTable.clear();
            entityBNodeTable.clear();
            while ((size = reader.read(buffer)) != -1) {
                nothingRead = false;
                for (int i = 0; i < size; ++i) {
                    if (i == 0 && buffer[i] == '#') {
                        lineNo++;
                        continue;
                    }
                    if (buffer[i] == lineSeparator) {
                        String line = String.copyValueOf(buffer, offset, i - offset);
                        if (eachLineIsRecord) {
                            processEachLineAsRecord(line);
                        } else {
                            emitGroupedSubjectsAsRecord(line);
                        }
                        offset = i + 1;
                        lineNo++;
                    }
                }
            }
            if (!nothingRead) {
                String line = String.copyValueOf(buffer, offset, buffer.length - offset);
                if (eachLineIsRecord) {
                    processEachLineAsRecord(line);
                    for (Element r : rootBnodes) {
                        r.serialise();
                    }
                } else {
                    emitGroupedSubjectsAsRecord(line);
                    for (Map.Entry<Entity, String> kv : entityBNodeTable.entrySet()) {
                        Entity bNode = nameBNodeTable.get(kv.getValue());
                        kv.getKey().addChildren(bNode.getChildren());
                    }
                    nameResourceTable.values().forEach(r -> r.replay(getReceiver()));
                }
            }

        } catch (final IOException e) {
            throw new MetafactureException(e);
        }

    }

    private void processEachLineAsRecord(String line) {
        getStatement(line, lineNo).ifPresent(statement -> {
            if (isBnode(statement.get(0))) {
                if (isBnode(statement.get(2))) {
                    Element entity = new Element(statement.get(1));
                    bnodeMap.get(statement.get(0)).addEntity(entity);
                    bnodeMap.put(statement.get(2), entity);
                } else {
                    Element literal = new Element(statement.get(1));
                    literal.addValue(statement.get(2));
                    bnodeMap.get(statement.get(0)).addEntity(literal);
                }
            } else if (isBnode(statement.get(2))) {
                Element elem = new Element(statement.get(1));
                bnodeMap.put(statement.get(2), elem);
                rootBnodes.add(elem);
            } else {
                this.getReceiver().startRecord(statement.get(0));
                this.getReceiver().literal(statement.get(1),
                        (unicodeEscapeSeq && statement.get(2).length() > 0)
                                ? toutf8(statement.get(2))
                                : statement.get(2));
                this.getReceiver().endRecord();
            }

        });
    }

    private void emitGroupedSubjectsAsRecord(String line) {
        getStatement(line, lineNo).ifPresent(statement -> {
            Entity resource;
            String subjectName = statement.get(0);
            String predicateName = statement.get(1);
            String objectName = statement.get(2);
            if (!isBnode(subjectName) && !nameResourceTable.containsKey(subjectName)) {
                resource = new Entity(subjectName);
                nameResourceTable.put(subjectName, resource);
            } else if (!isBnode(subjectName) && nameResourceTable.containsKey(subjectName)) {
                resource = nameResourceTable.get(subjectName);
            } else if (isBnode(subjectName) && !nameBNodeTable.containsKey(subjectName)) {
                resource = new Entity(subjectName);
                nameBNodeTable.put(subjectName, resource);
            } else {
                resource = nameBNodeTable.get(subjectName);
            }

            if (isBnode(objectName)) {
                Entity entity = resource.addEntity(predicateName);
                entityBNodeTable.put(entity, objectName);
            } else {
                resource.addLiteral(predicateName, (unicodeEscapeSeq && objectName.length() > 0)
                        ? toutf8(objectName)
                        : objectName);
            }
        });
    }

    private Optional<List<String>> getStatement(String line, int lineNo) {
        LOG.debug("Processing triple on line {}: {}", lineNo, line);
        Optional<List<String>> statement;
        try {
            statement = Optional.of(parseLine(line));
        } catch (Exception ex) {
            LOG.error("Parse error for triple on line {}: {}", lineNo, ex.getClass().getName());
            LOG.warn("Skipping triple on line {} because of error", lineNo);
            statement = Optional.empty();
        }
        return statement;
    }


    private static boolean isBnode(String str) {
        return str.startsWith("_:");
    }

    /**
     * Parses a N-triples statement and returns elements (subject, predicate, object) as three-part ArrayList
     *
     * @param line Statement to be parsed
     * @return List with subject, predicate and object
     */
    private List<String> parseLine(String line) throws MetafactureException {

        List<String> statement = new ArrayList<>();

        final byte NOCTX = 0;
        final byte INURI = 1;
        final byte INBNODE = 2;
        final byte INLITERAL = 3;
        final byte INIGNOREDCHAR = 4;
        final byte INDATATYPEURI = 5;
        final byte AFTERSINGLECARET = 6;
        final byte INLANGUAGETAG = 7;
        byte ctx = NOCTX;
        StringBuilder elem = new StringBuilder();

        Predicate beginOfUri = (char character, byte context) -> character == '<' && context == NOCTX;
        Predicate endOfUri = (char character, byte context) -> character == '>' && context == INURI;
        Predicate startOfLiteral = (char character, byte context) -> character == '"' && context == NOCTX;
        Predicate endOfLiteral = (char character, byte context) -> character == '"' && context == INLITERAL;
        Predicate startOfBNode = (char character, byte context) -> character == '_' && context == NOCTX;
        Predicate endOfBNode = (char character, byte context) -> (character == '.' || character == ' ' || character == '\t') && context == INBNODE;
        Predicate endOfTriple = (char character, byte context) -> character == '.' && context == NOCTX;
        Predicate escapeChar = (char character, byte context) -> character == 0x005c && context == INLITERAL;
        Predicate languageTag = (char character, byte context) -> character == 0x0040 && context == NOCTX;
        Predicate endOfLanguageTag = (char character, byte context) -> (character == '.' || character == ' ' || character == '\t') && context == INLANGUAGETAG;
        Predicate possibleDatatypeUri = (char character, byte context) -> character == '^' && context == NOCTX;
        Predicate noDatatypeUri = (char character, byte context) -> character != '^' && context == AFTERSINGLECARET;
        Predicate datatypeUri = (char character, byte context) -> character == '^' && context == AFTERSINGLECARET;
        Predicate endOfDatatypeUri = (char character, byte context) -> character == '>' && context == INDATATYPEURI;

        for (char c : line.toCharArray()) {

            if (is(beginOfUri, c, ctx)) {
                ctx = INURI;
            } else if (is(endOfUri, c, ctx)) {
                ctx = NOCTX;
                statement.add(elem.toString());
                elem.setLength(0);
            } else if (is(startOfLiteral, c, ctx)) {
                ctx = INLITERAL;
            } else if (is(endOfLiteral, c, ctx)) {
                ctx = NOCTX;
                statement.add(elem.toString());
                elem.setLength(0);
            } else if (is(startOfBNode, c, ctx)) {
                ctx = INBNODE;
                elem.append(c);
            } else if (is(endOfBNode, c, ctx)) {
                ctx = NOCTX;
                statement.add(elem.toString());
                elem.setLength(0);
            } else if (is(endOfTriple, c, ctx)) {
                break;
            } else if (is(escapeChar, c, ctx)) {
                ctx = INIGNOREDCHAR;
            } else if (is(languageTag, c, ctx)) {
                ctx = INLANGUAGETAG;
                elem.append("##");
                elem.append(c);
            } else if (is(possibleDatatypeUri, c, ctx)) {
                ctx = AFTERSINGLECARET;
                elem.append("##");
                elem.append(c);
            } else if (is(noDatatypeUri, c, ctx)) {
                elem.setLength(0);
            } else if (is(datatypeUri, c, ctx)) {
                ctx = INDATATYPEURI;
                elem.append(c);
            } else if (ctx == INIGNOREDCHAR) {
                if (c == '"') elem.append(c);
                if (c == 'u') {
                    elem.append("\\u");
                }
                ctx = INLITERAL;
            } else if (is(endOfLanguageTag, c, ctx)) {
                ctx = NOCTX;
                if (keepLanguageTags) {
                    statement.set(statement.size() - 1, statement.get(statement.size() - 1) + elem.toString());
                }
                elem.setLength(0);
            } else if (is(endOfDatatypeUri, c, ctx)) {
                ctx = NOCTX;
                elem.append(c);
                if (keepTypeAnnotations) {
                    statement.set(statement.size() - 1, statement.get(statement.size() - 1) + elem.toString());
                }
                elem.setLength(0);
            } else if (ctx == INURI || ctx == INLITERAL || ctx == INBNODE || ctx == INLANGUAGETAG || ctx == INDATATYPEURI) {                                                      // Record content
                elem.append(c);
            }

        }

        if (statement.size() != 3)
            throw (new MetafactureException("Statement must have exactly three elements: " + line));

        return statement;
    }

    private boolean is(Predicate p, char c, byte ctx) {
        return p.check(c, ctx);
    }

    interface Predicate {
        boolean check(char c, byte ctx);
    }


    class Element {
        String name;
        List<Element> elems = new ArrayList<>();
        String value;

        Element(String name) {
            this.name = name;
        }

        void addValue(String val) {
            this.value = val;
        }

        void addEntity(Element e) {
            this.elems.add(e);
        }

        void serialise(boolean asRoot) {
            if (value != null && !asRoot) {
                NtriplesDecoder.this.getReceiver().literal(this.name, this.value);
            } else if (elems.size() > 0) {
                if (asRoot) {
                    NtriplesDecoder.this.getReceiver().startRecord(name);
                } else {
                    NtriplesDecoder.this.getReceiver().startEntity(name);
                }
                for (Element e : elems) {
                    e.serialise(false);
                }
                if (asRoot) {
                    NtriplesDecoder.this.getReceiver().endRecord();
                } else {
                    NtriplesDecoder.this.getReceiver().endEntity();
                }
            }
        }

        void serialise() {
            this.serialise(true);
        }

    }
}
